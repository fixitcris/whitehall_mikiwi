﻿Imports System.ComponentModel
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Windows.Threading
Imports System.Threading

Class MainWindow
    'Regex function to remove " from string
    Dim r3 As New Regex("\x22{1}")
    'Bytes received from PLCs
    Dim bytes(1) As Byte
    'Bytes received from DCRs
    Dim DCRbytes(35) As Byte
    'Timer to check queue levels
    Public WithEvents Tick As New DispatcherTimer
    'Timer to check for new freshpak data for lanes that failed initial load
    Dim WithEvents Longtick As New DispatcherTimer
    'Timer to display status on log screen and write log files
    Dim WithEvents StatusTimer As New DispatcherTimer
    'Timer to keep comms alive
    Dim WithEvents CommsTimer As New DispatcherTimer
    'Initial API call
    Public WithEvents DropID_Call As New BackgroundWorker
    'Subsequent label calls
    Dim WithEvents Label_Call As New BackgroundWorker
    'Comms from PLCs
    Dim WithEvents Lane7_PLC_Server As New BackgroundWorker
    Dim WithEvents Lane9_PLC_Server As New BackgroundWorker
    Dim WithEvents Lane11_PLC_Server As New BackgroundWorker
    'Comms to printers
    Dim WithEvents Lane7_Client As New BackgroundWorker
    Dim WithEvents Lane9_Client As New BackgroundWorker
    Dim WithEvents Lane11_Client As New BackgroundWorker
    'Comms from DCRs
    Dim WithEvents Lane7_DCR_Server As New BackgroundWorker
    Dim WithEvents Lane9_DCR_Server As New BackgroundWorker
    Dim WithEvents Lane11_DCR_Server As New BackgroundWorker
    'Arrays to check REWE vs STD for ACK to PLCs
    Dim temparray1() As String
    Dim temparray2() As String
    Dim temparray3() As String
    'PLC request types, 0 = lane A, 1 = lane B
    Dim Request1 As Byte = &H0
    Dim Request2 As Byte = &H1

    Private Sub MainWindow_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        DropID_Call.WorkerSupportsCancellation = True
        Label_Call.WorkerSupportsCancellation = True
        Lane7_PLC_Server.WorkerSupportsCancellation = True
        Lane9_PLC_Server.WorkerSupportsCancellation = True
        Lane11_PLC_Server.WorkerSupportsCancellation = True
        Lane7_Client.WorkerSupportsCancellation = True
        Lane9_Client.WorkerSupportsCancellation = True
        Lane11_Client.WorkerSupportsCancellation = True
        Lane7_DCR_Server.WorkerSupportsCancellation = True
        Lane9_DCR_Server.WorkerSupportsCancellation = True
        Lane11_DCR_Server.WorkerSupportsCancellation = True
        'set from config file, live_test = true is live mode.
        Live_Test = My.Settings.Live_mode
        'Grader number as asigned by Radfords
        Grader = 1
        'Software version info for GUI
        VersionInfo.Text = "MiKiwi Ver." & My.Application.Info.Version.ToString & " © Copyright 2022, Mitech Limited"
        'Is settings window open?
        Set_open = False
        'set bit on initial load 
        First_cycle = True
        'Clear all fault flags
        Faults = {False, False, False, False, False, False}
        'run intial API calls
        DropID_Call.RunWorkerAsync()
        'Timer intervals
        Tick.Interval = New TimeSpan(0, 0, 1)
        Longtick.Interval = New TimeSpan(0, 1, 0)
        StatusTimer.Interval = New TimeSpan(0, 0, 1)
        CommsTimer.Interval = New TimeSpan(0, 0, 2)
        ' start dropID check timer
        Longtick.Start()
        'Hide Mitech Password box
        PWBox.Visibility = 1
        PWLabel.Visibility = 1
        'Load Lane/System allocations based on settings file (MiKiwi.exe.config)
        L7sys = My.Settings.L1system
        L9sys = My.Settings.L2system
        L11sys = My.Settings.L3system
        'Set all to first label flag and default layout and manual flags
        Lay = {1, 1, 1}
        Manual = {True, True, True}
        FirstSTD_label = {True, True, True, True, True, True}
        FirstSTDPAL_label = {True, True, True, True, True, True}
        FirstREWE_label = {True, True, True, True, True, True}
        FirstREWEPAL_label = {True, True, True, True, True, True}
    End Sub

    Private Sub Tick_Tick(sender As Object, e As EventArgs) Handles Tick.Tick
        'Check remaining amount of labels in each queue, and make API call if less than 1 label remains
        If Grader IsNot Nothing Then
            If L7_Labels.Count < My.Settings.Label_QueueSize And L7sys <> 0 And Faults(0) = False Then
                If Label_Call.IsBusy Then
                Else
                    Target = 7
                    Label_Call.RunWorkerAsync()
                End If
            ElseIf L9_Labels.Count < My.Settings.Label_QueueSize And L9sys <> 0 And Faults(2) = False Then
                If Label_Call.IsBusy Then
                Else
                    Target = 9
                    Label_Call.RunWorkerAsync()
                End If
            ElseIf L11_Labels.Count < My.Settings.Label_QueueSize And L11sys <> 0 And Faults(4) = False Then
                If Label_Call.IsBusy Then
                Else
                    Target = 11
                    Label_Call.RunWorkerAsync()
                End If
            End If
            If My.Settings.Split_Lanes = True Then
                If L8_Labels.Count < My.Settings.Label_QueueSize And L7sys <> 0 And Faults(1) = False Then
                    If Label_Call.IsBusy Then
                    Else
                        Target = 8
                        Label_Call.RunWorkerAsync()
                    End If
                ElseIf L10_Labels.Count < My.Settings.Label_QueueSize And L9sys <> 0 And Faults(3) = False Then
                    If Label_Call.IsBusy Then
                    Else
                        Target = 10
                        Label_Call.RunWorkerAsync()
                    End If
                ElseIf L12_Labels.Count < My.Settings.Label_QueueSize And L11sys <> 0 And Faults(5) = False Then
                    If Label_Call.IsBusy Then
                    Else
                        Target = 12
                        Label_Call.RunWorkerAsync()
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub Longtick_Tick(sender As Object, e As EventArgs) Handles Longtick.Tick
        'Call API for DROPIDs, if intial call is done, recalls any lanes that faulted during initial call
        If DropID_Call.IsBusy = False Then
            DropID_Call.RunWorkerAsync()
        End If
    End Sub
    Private Sub StatusTimer_Tick(sender As Object, e As EventArgs) Handles StatusTimer.Tick
        'updates label queue counts on GUI
        StatusText = "NULL"
        Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
    End Sub
    Private Sub DropID_Call_DoWork(sender As Object, e As DoWorkEventArgs) Handles DropID_Call.DoWork
        'set busy flag on to disable reset button.
        did_busy = True
        StatusText = "NULL"
        Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
        'If first cycle, Clear all label queues.
        If First_cycle = True Then
            If Live_Test = True Then
                StatusText = "Live mode"
                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                L7_Labels.Clear()
                L8_Labels.Clear()
                L9_Labels.Clear()
                L10_Labels.Clear()
                L11_Labels.Clear()
                L12_Labels.Clear()
            Else
                StatusText = "Test mode"
                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                'set DROPIDs to test ones as per radfords test environment
                L7_DropID = "001"
                L8_DropID = "001"
                L9_DropID = "002"
                L10_DropID = "002"
                L11_DropID = "003"
                L12_DropID = "003"
                L7_Labels.Clear()
                L8_Labels.Clear()
                L9_Labels.Clear()
                L10_Labels.Clear()
                L11_Labels.Clear()
                L12_Labels.Clear()
            End If
            StatusText = "Getting labels..."
            Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
            'Initial API calls for labels
            If L7sys <> 0 Then
                If L7_Labels.Count <> My.Settings.Label_QueueSize Then
                    Freshpak_Request(7)
                End If
            End If
            If L9sys <> 0 Then
                If L9_Labels.Count <> My.Settings.Label_QueueSize Then
                    Freshpak_Request(9)
                End If

            End If
            If L11sys <> 0 Then
                If L11_Labels.Count <> My.Settings.Label_QueueSize Then
                    Freshpak_Request(11)
                End If

            End If
            If My.Settings.Split_Lanes = True Then
                If L8_Labels.Count <> My.Settings.Label_QueueSize And L7sys <> 0 Then
                    Freshpak_Request(8)
                End If
                If L10_Labels.Count <> My.Settings.Label_QueueSize And L9sys <> 0 Then
                    Freshpak_Request(10)
                End If
                If L12_Labels.Count <> My.Settings.Label_QueueSize And L11sys <> 0 Then
                    Freshpak_Request(12)
                End If
            End If
            'if all required calls are good
            If Faults.Contains(True) = False Then
                    StatusText = "Required API Calls done, queues full."
                    Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                Else
                    'if any required calls are no good, set fault flags to failed lanes
                    Dim i As Boolean
                    Dim ii As Integer = 7
                    For Each i In Faults
                        Console.WriteLine("Lane " & ii & " fault = " & i)
                        If i = True Then
                            StatusText = "Lane " & ii & " API fault, Check Freshpak"
                            Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                        End If
                        ii += 1
                    Next
                End If
                'reset intial cycle bit, and start comms, queue checker and status updater
                First_cycle = False
                CommsTimer.Start()
                Tick.Start()
                StatusTimer.Start()
                'if intial bit is off...
            ElseIf First_cycle = False Then
                'API calls for any lanes that failed intital call for labels
                If Faults.Contains(True) = True Then
                StatusText = "Rechecking bad requests..."
                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                If Faults(0) = True Then
                    If Label_Call.IsBusy Then
                    Else
                        Target = 7
                        Label_Call.RunWorkerAsync()
                    End If
                End If
                If Faults(1) = True Then
                    If Label_Call.IsBusy Then
                    Else
                        Target = 8
                        Label_Call.RunWorkerAsync()
                    End If
                End If
                If Faults(2) = True Then
                    If Label_Call.IsBusy Then
                    Else
                        Target = 9
                        Label_Call.RunWorkerAsync()
                    End If
                End If
                If Faults(3) = True Then
                    If Label_Call.IsBusy Then
                    Else
                        Target = 10
                        Label_Call.RunWorkerAsync()
                    End If
                End If
                If Faults(4) = True Then
                    If Label_Call.IsBusy Then
                    Else
                        Target = 11
                        Label_Call.RunWorkerAsync()
                    End If
                End If
                If Faults(5) = True Then
                    If Label_Call.IsBusy Then
                    Else
                        Target = 12
                        Label_Call.RunWorkerAsync()
                    End If
                End If
            End If
        End If
        'set busy flag off to enable reset button.
        did_busy = False
    End Sub
    Public Sub DropID_Call_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles DropID_Call.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            'if BackgroundWorker terminated due to error
            Write_to_ERRORlog("DropID_Call:  " & e.Error.Message)
            DropID_Call.Dispose()
        ElseIf e.Cancelled Then
            'if background worker completes tasks
            DropID_Call.Dispose()
        End If
    End Sub
    'PLC comms to MiKiwi
    Public Sub Lane7_PLC_Server_DoWork(sender As Object, e As DoWorkEventArgs) Handles Lane7_PLC_Server.DoWork
        Lane7_PLC_Server.WorkerSupportsCancellation = True
        'Check which system is allocated to this lane
        If L7sys = 0 Then
            Lane7_PLC_Server.CancelAsync()
        ElseIf L7sys = 1 Then
            Port1 = My.Settings.MC1_PLC_PORT
        ElseIf L7sys = 2 Then
            Port1 = My.Settings.MC2_PLC_PORT
        ElseIf L7sys = 3 Then
            Port1 = My.Settings.MC3_PLC_PORT
        End If
        'Allocate correct port from above and start server.
        Server1 = New TcpListener(localAddr, Port1)
        Dim received1 As String
        Console.WriteLine("Lane 7 PLC Listen")
        Try
            Server1.Start()
        Catch exc As Exception
            Console.WriteLine("PLC Listener Lane 7 exception:" & exc.Message)
            Server1.Stop()
            Lane7_PLC_Server.CancelAsync()
            L7plcS = 0
        Finally
        End Try
        While True And Lane7_PLC_Server.CancellationPending = False
            client1 = Server1.AcceptTcpClient()
            Dim stream As NetworkStream = client1.GetStream()
            Dim i As Integer
            Array.Clear(bytes, 0, bytes.Length)
            While client1.Connected = True
                'if PLC is connected then set status flag
                L7plcS = 1
                'Timeout server connection after 30 seconds, protects against connection failure at power loss on PLC/Printer
                client1.ReceiveTimeout = 30000
                Console.WriteLine("Lane 7 PLC Connected")
                Try
                    If stream.CanRead Then
                        i = stream.Read(bytes, 0, bytes.Length)
                        If i > 0 Then
                            'start build cycle timer
                            start_time = Now
                            'convert received bytes from PLC to readable string
                            received1 = ByteArrayToString(bytes)
                            Console.WriteLine("Received from Lane 7 PLC: " & received1)
                            'if byte received from PLC is &H0 or &H1 then set label request as true.
                            If bytes(0) = Request1 Then 'Box is from first lane according to Kenntec
                                'check if label in queue is STD or REWE, based on content of REWE variable received from Freshpak, and send appropriate ACK if first label or not
                                If L7_Labels.Count <> 0 Then
                                    temparray1 = Split(L7_Labels(0), "|", -1)
                                    If temparray1(temparray1.Count - 4) = "" And temparray1(temparray1.Count - 5) <> "ull" Then
                                        If FirstSTD_label(0) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 7 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 7 ACK 0")
                                        End If
                                    ElseIf temparray1(temparray1.Count - 4) = "" And temparray1(temparray1.Count - 5) = "ull" Then
                                        If FirstSTDPAL_label(0) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 7 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 7 ACK 0")
                                        End If
                                    ElseIf temparray1(temparray1.Count - 4) <> "" And temparray1(temparray1.Count - 5) <> "ull" Then
                                        If FirstREWE_label(0) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 7 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 7 ACK 0")
                                        End If
                                    Else
                                        If FirstREWEPAL_label(0) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 7 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 7 ACK 0")
                                        End If
                                    End If
                                    'set request for print as true and close client
                                    L7Request = True
                                    Lane7_Client.RunWorkerAsync()
                                Else
                                    Write_to_ERRORlog("L7 PLC Server: " & "Request received from lane 7 but no label available")
                                    StatusText = "Request received from lane 7 but no label available"
                                    Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                                    client1.Close()
                                    Lane7_PLC_Server.CancelAsync()
                                End If
                                Exit While
                            ElseIf bytes(0) = Request2 Then 'Box is from second lane according to Kenntec
                                'check if label in queue is STD or REWE, based on content of REWE variable received from Freshpak, and send appropriate ACK if first label or not
                                If L8_Labels.Count <> 0 Then
                                    temparray1 = Split(L8_Labels(0), "|", -1)
                                    If temparray1(temparray1.Count - 4) = "" And temparray1(temparray1.Count - 5) <> "ull" Then
                                        If FirstSTD_label(0) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 8 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 8 ACK 0")
                                        End If
                                    ElseIf temparray1(temparray1.Count - 4) = "" And temparray1(temparray1.Count - 5) = "ull" Then
                                        If FirstSTDPAL_label(0) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 8 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 8 ACK 0")
                                        End If
                                    ElseIf temparray1(temparray1.Count - 4) <> "" And temparray1(temparray1.Count - 5) <> "ull" Then
                                        If FirstREWE_label(0) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 8 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 8 ACK 0")
                                        End If
                                    Else
                                        If FirstREWEPAL_label(0) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 8 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 8 ACK 0")
                                        End If
                                    End If
                                    'set request for print as true and close client
                                    L8Request = True
                                    Lane7_Client.RunWorkerAsync()
                                Else
                                    Write_to_ERRORlog("L7 PLC Server: " & "Request received from lane 8 but no label available")
                                    StatusText = "Request received from lane 8 but no label available"
                                    Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                                End If
                                client1.Close()
                                Lane7_PLC_Server.CancelAsync()
                                Exit While
                            End If
                        Else
                            Exit While
                        End If
                    End If
                Catch ex As Exception
                    Console.WriteLine("PLC Listener Lane 7 exception:" & ex.Message)
                    L7plcS = 0
                    Exit While
                End Try
                If Lane7_PLC_Server.CancellationPending = True Or Set_open = True Then
                    e.Cancel = True
                    Lane7_PLC_Server.Dispose()
                    client1.Close()
                    Exit While
                End If
            End While
            client1.Close()
            If Lane7_PLC_Server.CancellationPending = True Then
                e.Cancel = True
                Lane7_PLC_Server.Dispose()
                client1.Close()
                Exit While
            End If
        End While
        'Run client to send data to printer
        Server1.Stop()
        Lane7_PLC_Server.Dispose()
    End Sub
    Public Sub Lane7_PLC_Server_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles Lane7_PLC_Server.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            'if BackgroundWorker terminated due to error
            Write_to_ERRORlog("L7 PLC Server: " & e.Error.Message)
            Lane7_PLC_Server.Dispose()
        ElseIf e.Cancelled Then
            Lane7_PLC_Server.Dispose()
        Else
            'If Backgroundworker completes tasks
            Lane7_PLC_Server.Dispose()
            Console.WriteLine("Lane 7 PLC Close")
        End If
    End Sub
    Public Sub Lane9_PLC_Server_DoWork(sender As Object, e As DoWorkEventArgs) Handles Lane9_PLC_Server.DoWork
        Lane9_PLC_Server.WorkerSupportsCancellation = True
        'Check which system is allocated to this lane
        If L9sys = 0 Then
            Lane9_PLC_Server.CancelAsync()
        ElseIf L9sys = 1 Then
            Port2 = My.Settings.MC1_PLC_PORT
        ElseIf L9sys = 2 Then
            Port2 = My.Settings.MC2_PLC_PORT
        ElseIf L9sys = 3 Then
            Port2 = My.Settings.MC3_PLC_PORT
        End If
        'Allocate correct port from above and start server
        Server2 = New TcpListener(localAddr, Port2)
        Dim received2 As String
        Console.WriteLine("Lane 9 PLC Listen")
        Try
            Server2.Start()
        Catch exc As Exception
            Server2.Stop()
            Lane9_PLC_Server.CancelAsync()
            L9plcS = 0
        Finally
        End Try
        While True And Lane9_PLC_Server.CancellationPending = False
            client2 = Server2.AcceptTcpClient()
            Dim stream As NetworkStream = client2.GetStream()
            Dim i As Integer
            Array.Clear(bytes, 0, bytes.Length)
            While client2.Connected = True
                'if PLC is connected then set status flag
                L9plcS = 1
                'Timeout server connection after 30 seconds, protects against connection failure at power loss on PLC/Printer
                client2.ReceiveTimeout = 30000
                Console.WriteLine("Lane 9 PLC Connected")
                Try
                    If stream.CanRead Then
                        i = stream.Read(bytes, 0, bytes.Length)
                        If i > 0 Then
                            'start build cycle timer
                            start_time = Now
                            received2 = ByteArrayToString(bytes)
                            Console.WriteLine("Received from Lane 9 PLC: " & received2)
                            'if byte received from PLC is &H0 or &H1 then set label request as true.
                            If bytes(0) = Request1 Then 'Box is from first lane according to Kenntec
                                'check if label in queue is STD or REWE, based on content of REWE variable received from Freshpak, and send appropriate ACK if first label or not
                                If L9_Labels.Count <> 0 Then
                                    temparray2 = Split(L9_Labels(0), "|", -1)
                                    If temparray2(temparray2.Count - 4) = "" And temparray2(temparray2.Count - 5) <> "ull" Then
                                        If FirstSTD_label(1) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 9 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 9 ACK 0")
                                        End If
                                    ElseIf temparray2(temparray2.Count - 4) = "" And temparray2(temparray2.Count - 5) = "ull" Then
                                        If FirstSTDPAL_label(1) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 9 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 9 ACK 0")
                                        End If
                                    ElseIf temparray2(temparray2.Count - 4) <> "" And temparray2(temparray2.Count - 5) <> "ull" Then
                                        If FirstREWE_label(1) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 9 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 9 ACK 0")
                                        End If
                                    Else
                                        If FirstREWEPAL_label(1) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 9 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 9 ACK 0")
                                        End If
                                    End If
                                    L9Request = True
                                    Lane9_Client.RunWorkerAsync()
                                Else
                                    Write_to_ERRORlog("L9 PLC Server: " & "Request received from lane 9 but no label available")
                                    StatusText = "Request received from lane 9 but no label available"
                                    Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                                End If
                                client2.Close()
                                Lane9_PLC_Server.CancelAsync()
                                Exit While
                            ElseIf bytes(0) = Request2 Then
                                'check if label in queue is STD or REWE, based on content of REWE variable received from Freshpak, and send appropriate ACK if first label or not
                                If L10_Labels.Count <> 0 Then
                                    temparray2 = Split(L10_Labels(0), "|", -1)
                                    If temparray2(temparray2.Count - 4) = "" And temparray2(temparray2.Count - 5) <> "ull" Then
                                        If FirstSTD_label(1) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 10 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 10 ACK 0")
                                        End If
                                    ElseIf temparray2(temparray2.Count - 4) = "" And temparray2(temparray2.Count - 5) = "ull" Then
                                        If FirstSTDPAL_label(1) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 10 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 10 ACK 0")
                                        End If
                                    ElseIf temparray2(temparray2.Count - 4) <> "" And temparray2(temparray2.Count - 5) <> "ull" Then
                                        If FirstREWE_label(1) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 10 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 10 ACK 0")
                                        End If
                                    Else
                                        If FirstREWEPAL_label(1) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 10 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 10 ACK 0")
                                        End If
                                    End If
                                    L10Request = True
                                    Lane9_Client.RunWorkerAsync()
                                Else
                                    Write_to_ERRORlog("L9 PLC Server: " & "Request received from lane 10 but no label available")
                                    StatusText = "Request received from lane 10 but no label available"
                                    Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                                End If
                                client2.Close()
                                Lane9_PLC_Server.CancelAsync()
                                Exit While
                            End If
                        Else
                            Exit While
                        End If
                    End If
                Catch ex As Exception
                    Console.WriteLine("PLC Listener Lane 9 exception:" & ex.Message)
                    L9plcS = 0
                    Exit While
                End Try
                If Lane9_PLC_Server.CancellationPending = True Or Set_open = True Then
                    client2.Close()
                    Exit While
                End If
            End While
            client2.Close()
            If Lane9_PLC_Server.CancellationPending = True Then
                client2.Close()
                Exit While
            End If
        End While
        Server2.Stop()
        Lane9_PLC_Server.Dispose()
    End Sub
    Public Sub Lane9_PLC_Server_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles Lane9_PLC_Server.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            '' if BackgroundWorker terminated due to error
            Write_to_ERRORlog("L9 PLC Server: " & e.Error.Message)
            Lane9_PLC_Server.Dispose()
        ElseIf e.Cancelled Then
            Lane9_PLC_Server.Dispose()
        Else
            Lane9_PLC_Server.Dispose()
            Console.WriteLine("Lane 9 PLC Close")
        End If
    End Sub
    Public Sub Lane11_PLC_Server_DoWork(sender As Object, e As DoWorkEventArgs) Handles Lane11_PLC_Server.DoWork
        Lane11_PLC_Server.WorkerSupportsCancellation = True
        'Check which system is allocated to this lane
        If L11sys = 0 Then
            Lane11_PLC_Server.CancelAsync()
        ElseIf L11sys = 1 Then
            Port3 = My.Settings.MC1_PLC_PORT
        ElseIf L11sys = 2 Then
            Port3 = My.Settings.MC2_PLC_PORT
        ElseIf L11sys = 3 Then
            Port3 = My.Settings.MC3_PLC_PORT
        End If
        'Allocate correct port from above and start server. If server starts successfully, set status on screen to OK, else NOK
        Server3 = New TcpListener(localAddr, Port3)
        Dim received3 As String
        Console.WriteLine("Lane 11 PLC Listen")
        Try
            Server3.Start()
        Catch exc As Exception
            Server3.Stop()
            Lane11_PLC_Server.CancelAsync()
            L11plcS = 0
        Finally
        End Try
        While True And Lane11_PLC_Server.CancellationPending = False
            client3 = Server3.AcceptTcpClient()
            Dim stream As NetworkStream = client3.GetStream()
            Dim i As Integer
            Array.Clear(bytes, 0, bytes.Length)
            While client3.Connected = True
                L11plcS = 1
                'Timeout server connection after 30 seconds, protects against connection failure at power loss on PLC/Printer
                client3.ReceiveTimeout = 30000
                Console.WriteLine("Lane 11 PLC Connected")
                Try
                    If stream.CanRead Then
                        i = stream.Read(bytes, 0, bytes.Length)
                        If i > 0 Then
                            received3 = ByteArrayToString(bytes)
                            'Console.Beep(2000, 200)
                            start_time = Now
                            Console.WriteLine("Received from Lane 11 PLC: " & received3)
                            'if byte received from PLC is &H0 or &H1 then set label request as true.
                            If bytes(0) = Request1 Then
                                If L11_Labels.Count <> 0 Then
                                    temparray3 = Split(L11_Labels(0), "|", -1)
                                    If temparray3(temparray3.Count - 4) = "" And temparray3(temparray3.Count - 5) <> "ull" Then
                                        If FirstSTD_label(2) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 11 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 11 ACK 0")
                                        End If
                                    ElseIf temparray3(temparray3.Count - 4) = "" And temparray3(temparray3.Count - 5) = "ull" Then
                                        If FirstSTDPAL_label(2) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 11 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 11 ACK 0")
                                        End If
                                    ElseIf temparray3(temparray3.Count - 4) <> "" And temparray3(temparray3.Count - 5) <> "ull" Then
                                        If FirstREWE_label(2) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 11 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 11 ACK 0")
                                        End If
                                    Else
                                        If FirstREWEPAL_label(2) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 11 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 11 ACK 0")
                                        End If
                                    End If
                                    L11Request = True
                                    Lane11_Client.RunWorkerAsync()
                                Else
                                    Write_to_ERRORlog("L11 PLC Server: " & "Request received from lane 11 but no label available")
                                    StatusText = "Request received from lane 11 but no label available"
                                    Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                                End If
                                client3.Close()
                                Lane11_PLC_Server.CancelAsync()
                                Exit While
                            ElseIf bytes(0) = Request2 Then
                                If L12_Labels.Count <> 0 Then
                                    temparray3 = Split(L12_Labels(0), "|", -1)
                                    If temparray3(temparray3.Count - 4) = "" And temparray3(temparray3.Count - 5) <> "ull" Then
                                        If FirstSTD_label(2) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 12 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 12 ACK 0")
                                        End If
                                    ElseIf temparray3(temparray3.Count - 4) = "" And temparray3(temparray3.Count - 5) = "ull" Then
                                        If FirstSTDPAL_label(2) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 12 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 12 ACK 0")
                                        End If
                                    ElseIf temparray3(temparray3.Count - 4) <> "" And temparray3(temparray3.Count - 5) <> "ull" Then
                                        If FirstREWE_label(2) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 12 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 12 ACK 0")
                                        End If
                                    Else
                                        If FirstREWEPAL_label(2) = True Then
                                            stream.Write(ACK1, 0, ACK1.Length)
                                            Console.WriteLine("Lane 12 ACK 1")
                                        Else
                                            stream.Write(ACK0, 0, ACK0.Length)
                                            Console.WriteLine("Lane 12 ACK 0")
                                        End If
                                    End If
                                    L12Request = True
                                    Lane11_Client.RunWorkerAsync()
                                Else
                                    Write_to_ERRORlog("L11 PLC Server: " & "Request received from lane 12 but no label available")
                                    StatusText = "Request received from lane 12 but no label available"
                                    Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                                End If
                                client3.Close()
                                Lane11_PLC_Server.CancelAsync()
                                Exit While
                            End If
                        Else
                            Exit While
                        End If
                    End If
                Catch ex As Exception
                    Console.WriteLine("PLC Listener Lane 11 exception: " & ex.Message)
                    L11plcS = 0
                    Exit While
                End Try
                If Lane11_PLC_Server.CancellationPending = True Or Set_open = True Then
                    client3.Close()
                    Exit While
                End If
            End While
            client3.Close()
            If Lane11_PLC_Server.CancellationPending = True Then
                client3.Close()
                Exit While
            End If
        End While
        Server3.Stop()
        Lane11_PLC_Server.Dispose()
    End Sub
    Public Sub Lane11_PLC_Server_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles Lane11_PLC_Server.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            'if BackgroundWorker terminated due to error
            Write_to_ERRORlog("L11 PLC Server: " & e.Error.Message)
            Lane11_PLC_Server.Dispose()
        ElseIf e.Cancelled Then
            Lane11_PLC_Server.Dispose()
        Else
            Lane11_PLC_Server.Dispose()
            Console.WriteLine("Lane 11 PLC Close")
        End If
    End Sub
    'MiKiwi comms to Printers
    Public Sub Lane7_Client_DoWork(sender As Object, e As DoWorkEventArgs) Handles Lane7_Client.DoWork
        'Check which system is allocated to this lane
        Using L1Client As New TcpClient
            If L7sys = 0 Then
                Lane7_Client.CancelAsync()
                e.Cancel = True
            ElseIf L7sys = 1 Then
                L7ip = IPAddress.Parse(My.Settings.MC1_Printer_IP)
                L7ipstr = My.Settings.MC1_Printer_IP
            ElseIf L7sys = 2 Then
                L7ip = IPAddress.Parse(My.Settings.MC2_Printer_IP)
                L7ipstr = My.Settings.MC2_Printer_IP
            ElseIf L7sys = 3 Then
                L7ip = IPAddress.Parse(My.Settings.MC3_Printer_IP)
                L7ipstr = My.Settings.MC3_Printer_IP
            End If
            L1Client.ReceiveTimeout = 5000
            Try
                If Lane7_Client.CancellationPending = False And Set_open = False Then
                    Dim port As Integer = 9100
                    L1Client.Connect(L7ip, port)
                Else
                    L7prS = 0
                    Exit Try
                End If
                While True
                    L7prS = 1
                    Dim stream As NetworkStream = L1Client.GetStream()
                    If L7Request = True Then
                        Try
                            L1Client.SendTimeout = 8000
                            If stream.CanWrite Then
                                stream.Write(Convert_to_send(7), 0, Bytes_to_send.Length)
                                StatusText = "Sent to L7 printer: " & Join(Variable_Array, ", ")
                                stop_time = Now
                                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                                Exit While
                            End If
                        Catch ex As Exception
                            Console.WriteLine("Lane 7 printer: " & ex.Message)
                            Exit While
                        Finally
                            L7Request = False
                        End Try
                    ElseIf L8Request = True Then
                        Try
                            L1Client.SendTimeout = 8000
                            If stream.CanWrite Then
                                stream.Write(Convert_to_send(8), 0, Bytes_to_send.Length)
                                StatusText = "Sent to L8 printer: " & Join(Variable_Array, ", ")
                                stop_time = Now
                                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                                Exit While
                            End If
                        Catch ex As Exception
                            Console.WriteLine("Lane 8 printer: " & ex.Message)
                            Exit While
                        Finally
                            L8Request = False
                        End Try
                    End If
                    If Lane7_Client.CancellationPending = True Then
                        Exit While
                    End If
                End While
            Catch ex As SocketException
                FirstSTD_label(0) = True
                FirstREWE_label(0) = True
                L7prS = False
                L7Request = False
                L8Request = False
                Write_to_ERRORlog("Lane 7 & 8 printer: " & ex.Message)
                StatusText = "Request received from Lane 7 & 8 PLC, But printer unreachable"
                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
            Finally
                Thread.Sleep(600)
                L1Client.Close()
            End Try
        End Using
    End Sub
    Public Sub Lane7_Client_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles Lane7_Client.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            'if BackgroundWorker terminated due to error
            Write_to_ERRORlog("L7 Printer Client: " & e.Error.Message)
            Lane7_Client.Dispose()
        ElseIf e.Cancelled Then
            Lane7_Client.Dispose()
        Else
            Lane7_Client.Dispose()
            Console.WriteLine("Lane 7 Printer done")
        End If
    End Sub
    Public Sub Lane9_Client_DoWork(sender As Object, e As DoWorkEventArgs) Handles Lane9_Client.DoWork
        'Check which system is allocated to this lane
        Using L2Client As New TcpClient
            If L9sys = 0 Then
                Lane9_Client.CancelAsync()
                e.Cancel = True
            ElseIf L9sys = 1 Then
                L9ip = IPAddress.Parse(My.Settings.MC1_Printer_IP)
                L9ipstr = My.Settings.MC1_Printer_IP
            ElseIf L9sys = 2 Then
                L9ip = IPAddress.Parse(My.Settings.MC2_Printer_IP)
                L9ipstr = My.Settings.MC2_Printer_IP
            ElseIf L9sys = 3 Then
                L9ip = IPAddress.Parse(My.Settings.MC3_Printer_IP)
                L9ipstr = My.Settings.MC3_Printer_IP
            End If
            L2Client.ReceiveTimeout = 5000
            Try
                If Lane9_Client.CancellationPending = False And Set_open = False Then
                    Dim port As Integer = 9100
                    L2Client.Connect(L9ip, port)
                Else
                    L9prS = 0
                    Exit Try
                End If
                While True
                    L9prS = 1
                    Dim stream As NetworkStream = L2Client.GetStream()
                    If L9Request = True Then
                        Try
                            L2Client.SendTimeout = 8000
                            If stream.CanWrite Then
                                stream.Write(Convert_to_send(9), 0, Bytes_to_send.Length)
                                StatusText = "Sent to L9 printer: " & Join(Variable_Array, ", ")
                                stop_time = Now
                                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                                Exit While
                            End If
                        Catch ex As Exception
                            Console.WriteLine("Lane 9 printer: " & ex.Message)
                            Exit While
                        Finally
                            L9Request = False
                        End Try
                    ElseIf L10Request = True Then
                        Try
                            L2Client.SendTimeout = 8000
                            If stream.CanWrite Then
                                stream.Write(Convert_to_send(10), 0, Bytes_to_send.Length)
                                StatusText = "Sent to L10 printer: " & Join(Variable_Array, ", ")
                                stop_time = Now
                                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                                Exit While
                            End If
                        Catch ex As Exception
                            Console.WriteLine("Lane 10 printer: " & ex.Message)
                            Exit While
                        Finally
                            L10Request = False
                        End Try
                    End If
                    If Lane9_Client.CancellationPending = True Then
                        Exit While
                    End If
                End While
            Catch ex As SocketException
                FirstSTD_label(1) = True
                FirstREWE_label(1) = True
                L9prS = False
                L9Request = False
                L10Request = False
                Write_to_ERRORlog("Lane 9 & 10 printer: " & ex.Message)
                StatusText = "Request received from Lane 9 & 10 PLC, But printer unreachable"
                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
            Finally
                Thread.Sleep(600)
                L2Client.Close()
            End Try
        End Using
    End Sub
    Public Sub Lane9_Client_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles Lane9_Client.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            'if BackgroundWorker terminated due to error
            'MessageBox.Show("Lanes 9 & 10 Client: " & e.Error.Message)
            Write_to_ERRORlog("L9 Printer Client: " & e.Error.Message)
            Lane9_Client.Dispose()
        ElseIf e.Cancelled Then
            Lane9_Client.Dispose()
        Else
            Lane9_Client.Dispose()
            Console.WriteLine("Lane 9 Printer done")
        End If
    End Sub
    Public Sub Lane11_Client_DoWork(sender As Object, e As DoWorkEventArgs) Handles Lane11_Client.DoWork
        'Check which system is allocated to this lane
        Using L3Client As New TcpClient
            If L11sys = 0 Then
                Lane11_Client.CancelAsync()
                e.Cancel = True
            ElseIf L11sys = 1 Then
                L11ip = IPAddress.Parse(My.Settings.MC1_Printer_IP)
                L11ipstr = My.Settings.MC1_Printer_IP
            ElseIf L11sys = 2 Then
                L11ip = IPAddress.Parse(My.Settings.MC2_Printer_IP)
                L11ipstr = My.Settings.MC2_Printer_IP
            ElseIf L11sys = 3 Then
                L11ip = IPAddress.Parse(My.Settings.MC3_Printer_IP)
                L11ipstr = My.Settings.MC3_Printer_IP
            End If
            L3Client.ReceiveTimeout = 5000
            Try
                If Lane11_Client.CancellationPending = False And Set_open = False Then
                    Dim port As Integer = 9100
                    L3Client.Connect(L11ip, port)
                Else
                    L11prS = 0
                    Exit Try
                End If
                While True
                    L11prS = 1
                    Dim stream As NetworkStream = L3Client.GetStream()
                    If L11Request = True Then
                        Try
                            L3Client.SendTimeout = 8000
                            If stream.CanWrite Then
                                stream.Write(Convert_to_send(11), 0, Bytes_to_send.Length)
                                StatusText = "Sent to L11 printer: " & Join(Variable_Array, ", ")
                                stop_time = Now
                                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                                Exit While
                            End If
                        Catch ex As Exception
                            Console.WriteLine("Lane 11 printer: " & ex.Message)
                            Exit While
                        Finally
                            L11Request = False
                        End Try
                    ElseIf L12Request = True Then
                        Try
                            L3Client.SendTimeout = 8000
                            If stream.CanWrite Then
                                stream.Write(Convert_to_send(12), 0, Bytes_to_send.Length)
                                StatusText = "Sent to L12 printer: " & Join(Variable_Array, ", ")
                                stop_time = Now
                                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                                Exit While
                            End If
                        Catch ex As Exception
                            Console.WriteLine("Lane 12 printer: " & ex.Message)
                            Exit While
                        Finally
                            L12Request = False
                        End Try
                    End If
                    If Lane11_Client.CancellationPending = True Then
                        Exit While
                    End If
                End While
            Catch ex As SocketException
                L11prS = False
                FirstSTD_label(2) = True
                FirstREWE_label(2) = True
                L11Request = False
                L12Request = False
                Write_to_ERRORlog("Lane 11 & 12 printer: " & ex.Message)
                StatusText = "Request received from Lane 11 & 12 PLC, But printer unreachable"
                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
            Finally
                Thread.Sleep(600)
                L3Client.Close()
            End Try
        End Using
    End Sub
    Public Sub Lane11_Client_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles Lane11_Client.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            'if BackgroundWorker terminated due to error
            Write_to_ERRORlog("L11 Printer Client: " & e.Error.Message)
            Lane11_Client.Dispose()
        ElseIf e.Cancelled Then
            Lane11_Client.Dispose()
        Else
            Lane11_Client.Dispose()
            Console.WriteLine("Lane 11 Printer done")
        End If
    End Sub
    'DCR comms to MiKiwi
    Public Sub Lane7_DCR_Server_DoWork(sender As Object, e As DoWorkEventArgs) Handles Lane7_DCR_Server.DoWork
        Lane7_DCR_Server.WorkerSupportsCancellation = True
        'Check which system is allocated to this lane
        L7sys = My.Settings.L1system
        Select Case L7sys
            Case 0
                Lane7_DCR_Server.CancelAsync()
                e.Cancel = True
            Case 1
                Port4 = My.Settings.MC1_DCR_PORT
            Case 2
                Port4 = My.Settings.MC2_DCR_PORT
            Case 3
                Port4 = My.Settings.MC3_DCR_PORT
        End Select
        'Allocate correct port from above and start server. If server starts successfully, set status on screen to OK, else NOK
        Server4 = New TcpListener(localAddr, Port4)
        Dim DCRreceived1 As String
        Try
            Server4.Start()
            StatusText = "NULL"
            Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
        Catch exc As Exception
            Server4.Stop()
            Lane7_DCR_Server.CancelAsync()
            L7dcrS = 0
        End Try
        While True And Lane7_DCR_Server.CancellationPending = False And Set_open = False
            client4 = Server4.AcceptTcpClient()
            Dim stream As NetworkStream = client4.GetStream()
            Dim i As Integer
            While True
                L7dcrS = 1
                Try
                    If stream.CanRead Then
                        i = stream.Read(DCRbytes, 0, DCRbytes.Length)
                        If DCRbytes.Length > 0 Then
                            DCRreceived1 = AsciiBytesToString(DCRbytes)
                            If DCRbytes(0) = &H3F Then
                                StatusText = "Scan from Lanes 7 & 8: NO SCAN"
                                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                            Else
                                StatusText = "Scan from Lanes 7 & 8: " & DCRreceived1
                                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                            End If
                            Exit While
                        End If
                    End If
                Catch ex As Exception
                    Console.WriteLine("DCR 7 Exception: " & ex.Message)
                    Exit While
                End Try
                If Lane7_DCR_Server.CancellationPending = True Or Set_open = True Then
                    client4.Close()
                    Exit While
                End If
            End While
            client4.Close()
            If Lane7_DCR_Server.CancellationPending = True Then
                client4.Close()
                Exit While
            End If
        End While
        Thread.Sleep(2000)
        Server5.Stop()
    End Sub
    Public Sub Lane7_DCR_Server_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles Lane7_DCR_Server.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            'if BackgroundWorker terminated due to error
            'MessageBox.Show("Lanes 7 & 8 DCR Server: " & e.Error.Message)
            Write_to_ERRORlog("L7 DCR Server: " & e.Error.Message)
            Lane7_DCR_Server.Dispose()
        ElseIf e.Cancelled Then
            Lane7_DCR_Server.Dispose()
        Else
            Lane7_DCR_Server.Dispose()
            Console.WriteLine("Lane 7 DCR done")
        End If
    End Sub
    Public Sub Lane9_DCR_Server_DoWork(sender As Object, e As DoWorkEventArgs) Handles Lane9_DCR_Server.DoWork
        Lane9_DCR_Server.WorkerSupportsCancellation = True
        'Check which system is allocated to this lane
        L9sys = My.Settings.L2system
        Select Case L9sys
            Case 0
                Lane9_DCR_Server.CancelAsync()
                e.Cancel = True
            Case 1
                Port5 = My.Settings.MC1_DCR_PORT
            Case 2
                Port5 = My.Settings.MC2_DCR_PORT
            Case 3
                Port5 = My.Settings.MC3_DCR_PORT
        End Select
        'Allocate correct port from above and start server. If server starts successfully, set status on screen to OK, else NOK
        Server5 = New TcpListener(localAddr, Port5)
        Dim DCRreceived2 As String
        Try
            Server5.Start()
            StatusText = "NULL"
            Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
        Catch exc As Exception
            Server5.Stop()
            Lane9_DCR_Server.CancelAsync()
            L9dcrS = 0
        End Try
        While True And Lane9_DCR_Server.CancellationPending = False And Set_open = False
            client5 = Server5.AcceptTcpClient()
            Dim stream As NetworkStream = client5.GetStream()
            Dim i As Integer
            While True
                L9dcrS = 1
                Try
                    If stream.CanRead Then
                        i = stream.Read(DCRbytes, 0, DCRbytes.Length)
                        If DCRbytes.Length > 0 Then
                            DCRreceived2 = AsciiBytesToString(DCRbytes)
                            If DCRbytes(0) = &H3F Then
                                StatusText = "Scan from Lanes 9 & 10: NO SCAN"
                                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                            Else
                                StatusText = "Scan from Lanes 9 & 10: " & DCRreceived2
                                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                            End If
                            Exit While
                        End If
                    End If
                Catch ex As Exception
                    Console.WriteLine("DCR 9 Exception: " & ex.Message)
                    Exit While
                End Try
                If Lane9_DCR_Server.CancellationPending = True Or Set_open = True Then
                    client5.Close()
                    Exit While
                End If
            End While
            client5.Close()
            If Lane9_DCR_Server.CancellationPending = True Then
                client5.Close()
                Exit While
            End If
        End While
        Thread.Sleep(2000)
        Server5.Stop()
    End Sub
    Public Sub Lane9_DCR_Server_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles Lane9_DCR_Server.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            'if BackgroundWorker terminated due to error
            Write_to_ERRORlog("L9 DCR Server: " & e.Error.Message)
            Lane9_DCR_Server.Dispose()
        ElseIf e.Cancelled Then
            Lane9_DCR_Server.Dispose()
        Else
            Lane9_DCR_Server.Dispose()
            Console.WriteLine("Lane 9 DCR done")
        End If
    End Sub
    Public Sub Lane11_DCR_Server_DoWork(sender As Object, e As DoWorkEventArgs) Handles Lane11_DCR_Server.DoWork
        Lane11_DCR_Server.WorkerSupportsCancellation = True
        'Check which system is allocated to this lane
        L11sys = My.Settings.L3system
        Select Case L11sys
            Case 0
                Lane11_DCR_Server.CancelAsync()
                e.Cancel = True
            Case 1
                Port6 = My.Settings.MC1_DCR_PORT
            Case 2
                Port6 = My.Settings.MC2_DCR_PORT
            Case 3
                Port6 = My.Settings.MC3_DCR_PORT
        End Select
        'Allocate correct port from above and start server. If server starts successfully, set status on screen to OK, else NOK
        Server6 = New TcpListener(localAddr, Port6)
        Dim DCRreceived3 As String
        Try
            Server6.Start()
            StatusText = "NULL"
            Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
        Catch esc As Exception
            Server6.Stop()
            Lane11_DCR_Server.CancelAsync()
            L11dcrS = 0
        End Try
        While True And Lane11_DCR_Server.CancellationPending = False And Set_open = False
            client6 = Server6.AcceptTcpClient()
            Dim stream As NetworkStream = client6.GetStream()
            Dim i As Integer
            While True
                L11dcrS = 1
                Try
                    If stream.CanRead Then
                        i = stream.Read(DCRbytes, 0, DCRbytes.Length)
                        If DCRbytes.Length > 0 Then
                            DCRreceived3 = AsciiBytesToString(DCRbytes)
                            If DCRbytes(0) = &H3F Then
                                StatusText = "Scan from Lanes 11 & 12: NO SCAN"
                                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                            Else
                                StatusText = "Scan from Lanes 11 & 12: " & DCRreceived3
                                Dispatcher.Invoke(DispatcherPriority.Send, New Action(AddressOf Update_Status))
                            End If
                            Exit While
                        End If
                    End If
                Catch ex As Exception
                    Console.WriteLine("DCR 11 Exception: " & ex.Message)
                    Exit While
                End Try
                If Lane11_DCR_Server.CancellationPending = True Or Set_open = True Then
                    client6.Close()
                    Exit While
                End If
            End While
            client6.Close()
            If Lane11_DCR_Server.CancellationPending = True Then
                client6.Close()
                Exit While
            End If
        End While
        Thread.Sleep(2000)
        Server6.Stop()
    End Sub
    Public Sub Lane11_DCR_Server_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles Lane11_DCR_Server.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            'if BackgroundWorker terminated due to error
            Write_to_ERRORlog("L11 DCR Server: " & e.Error.Message)
            Lane11_DCR_Server.Dispose()
        ElseIf e.Cancelled Then
            Lane11_DCR_Server.Dispose()
        Else
            Lane11_DCR_Server.Dispose()
            Console.WriteLine("Lane 11 DCR done")
        End If
    End Sub
    'Open lane allocation window, allows user to assign system to lanes. Cancels all running comms workers to allow ports to close and change. May take several seconds to return to normal.
    Private Sub Settings_btn_Click(sender As Object, e As RoutedEventArgs) Handles Settings_btn.Click
        Dim Laneset As New LaneSettings()
        Set_open = True
        Lane7_PLC_Server.CancelAsync()
        Lane9_PLC_Server.CancelAsync()
        Lane11_PLC_Server.CancelAsync()
        Lane7_Client.CancelAsync()
        Lane9_Client.CancelAsync()
        Lane11_Client.CancelAsync()
        Lane7_DCR_Server.CancelAsync()
        Lane9_DCR_Server.CancelAsync()
        Lane11_DCR_Server.CancelAsync()
        Laneset.Show()
    End Sub
    Private Sub Freshpak_Request(target As Integer)
        'Initial API CALL for labels, repeats for any API Faults
        Select Case target
            Case 7
                DropID = L7_DropID
                Do
                    Try
                        'API Call to Freshpak for label data. Loops until queue is full.
                        If Live_Test = False Then
                            API_CALL = APITest & API_Call1 & "print/" & DropID & API_Call2
                        Else
                            API_CALL = Live_API & API_Call1 & "print/" & DropID & API_Call2
                        End If
                        'If Live_Test = True Then
                        Console.WriteLine("L7 API CALL String: " & API_CALL)
                            Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                            Dim response As WebResponse = APICall.GetResponse()
                            Dim dataStream As Stream = response.GetResponseStream()
                            Dim Reader As New StreamReader(dataStream)
                            API_response = Reader.ReadToEnd()
                            'Else
                            '    API_response = File.ReadAllText("C:\Users\User\Desktop\Customer Files\Whitehall\Logs\APILOG.txt")
                            'End If
                            If API_response.Contains("Bad") = True Then
                            StatusText = "Lane 7 incorrect Freshpak data"
                            Reader.Close()
                            response.Close()
                            Exit Do
                        End If
                        API_response = r3.Replace(API_response, "")
                        Dim tempSplit() As String = Split(API_response, ",", -1)
                        Console.WriteLine("Unsplit = " & Join(tempSplit, "|"))
                        Reader.Close()
                        response.Close()
                        'Concat all received data that is required for label printing and add to list
                        L7_Labels.Add(Strings.Left(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & Right(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & tempSplit(7).Substring(tempSplit(7).IndexOf(":"c) + 1) & "|" & tempSplit(5).Substring(tempSplit(5).IndexOf(":"c) + 1) & "|" & tempSplit(8).Substring(tempSplit(8).IndexOf(":"c) + 1) & "|" & tempSplit(23).Substring(tempSplit(23).IndexOf(":"c) + 1) & "|" & tempSplit(22).Substring(tempSplit(22).IndexOf(":"c) + 1) & "|" & tempSplit(9).Substring(tempSplit(9).IndexOf(":"c) + 1) & "|" & tempSplit(24).Substring(tempSplit(24).IndexOf(":"c) + 1) & "|" & tempSplit(2).Substring(tempSplit(2).IndexOf(":"c) + 1) & "|" & tempSplit(3).Substring(tempSplit(3).IndexOf(":"c) + 1) & "|" & DropID & "|" & Replace(tempSplit(20).Substring(tempSplit(20).IndexOf(":"c) + 1), "}", "") & "|" & Replace(tempSplit(47).Substring(tempSplit(47).IndexOf(":"c) + 1), "}", "") & "|" & tempSplit(46).Substring(tempSplit(46).IndexOf(":"c) + 1) & "|" & " L7" & "|" & tempSplit(My.Settings.Pallet_No_Index).Substring(tempSplit(My.Settings.Pallet_No_Index).IndexOf(":"c) + 1) & "|" & tempSplit(15).Substring(tempSplit(15).IndexOf(":"c) + 1))
                    Catch ex As Exception
                        Console.WriteLine("Lane 7 API call: " & ex.Message)
                        StatusText = "Lane 7 API call: " & ex.Message
                        Write_to_ERRORlog("L7 API call initial: " & ex.Message)
                        Exit Do
                    End Try
                Loop Until L7_Labels.Count > My.Settings.Label_QueueSize - 1 < My.Settings.Label_QueueSize + 1
                If L7_Labels.Count <> 0 Then
                    Faults(0) = False
                Else
                    StatusText = "Lane 7 API call failed"
                    Faults(0) = True
                End If
            Case 8
                DropID = L8_DropID
                Do
                    Try
                        'API Call to Freshpak for label data. Loops queue is full.
                        If Live_Test = False Then
                            API_CALL = APITest & API_Call1 & "print/" & DropID & API_Call2
                        Else
                            API_CALL = Live_API & API_Call1 & "print/" & DropID & API_Call2
                        End If
                        Console.WriteLine("L8 API CALL String: " & API_CALL)
                        Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                        Dim response As WebResponse = APICall.GetResponse()
                        Dim dataStream As Stream = response.GetResponseStream()
                        Dim Reader As New StreamReader(dataStream)
                        API_response = Reader.ReadToEnd()
                        If API_response.Contains("Bad") = True Then
                            StatusText = "Lane 8 incorrect Freshpak data"
                            Reader.Close()
                            response.Close()
                            Exit Do
                        End If
                        API_response = r3.Replace(API_response, "")
                        Dim tempSplit() As String = Split(API_response, ",", -1)
                        Console.WriteLine("Unsplit = " & Join(tempSplit, "|"))
                        Reader.Close()
                        response.Close()
                        'Concat all received data that is required for label printing and add to list
                        L8_Labels.Add(Strings.Left(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & Right(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & tempSplit(7).Substring(tempSplit(7).IndexOf(":"c) + 1) & "|" & tempSplit(5).Substring(tempSplit(5).IndexOf(":"c) + 1) & "|" & tempSplit(8).Substring(tempSplit(8).IndexOf(":"c) + 1) & "|" & tempSplit(23).Substring(tempSplit(23).IndexOf(":"c) + 1) & "|" & tempSplit(22).Substring(tempSplit(22).IndexOf(":"c) + 1) & "|" & tempSplit(9).Substring(tempSplit(9).IndexOf(":"c) + 1) & "|" & tempSplit(24).Substring(tempSplit(24).IndexOf(":"c) + 1) & "|" & tempSplit(2).Substring(tempSplit(2).IndexOf(":"c) + 1) & "|" & tempSplit(3).Substring(tempSplit(3).IndexOf(":"c) + 1) & "|" & DropID & "|" & Replace(tempSplit(20).Substring(tempSplit(20).IndexOf(":"c) + 1), "}", "") & "|" & Replace(tempSplit(47).Substring(tempSplit(47).IndexOf(":"c) + 1), "}", "") & "|" & tempSplit(46).Substring(tempSplit(46).IndexOf(":"c) + 1) & "|" & " L8" & "|" & tempSplit(My.Settings.Pallet_No_Index).Substring(tempSplit(My.Settings.Pallet_No_Index).IndexOf(":"c) + 1) & "|" & tempSplit(15).Substring(tempSplit(15).IndexOf(":"c) + 1))
                    Catch ex As Exception
                        Console.WriteLine("Lane 8 API call: " & ex.Message)
                        StatusText = "Lane 8 API call: " & ex.Message
                        Write_to_ERRORlog("L8 API call initial: " & ex.Message)
                        Exit Do
                    End Try
                Loop Until L8_Labels.Count > My.Settings.Label_QueueSize - 1 < My.Settings.Label_QueueSize + 1
                If L8_Labels.Count <> 0 Then
                    Faults(1) = False
                Else
                    StatusText = "Lane 8 API call failed"
                    Faults(1) = True
                End If
            Case 9
                DropID = L9_DropID
                Do
                    Try
                        'API Call to Freshpak for label data. Loops until queue is full.
                        If Live_Test = False Then
                            API_CALL = APITest & API_Call1 & "print/" & DropID & API_Call2
                        Else
                            API_CALL = Live_API & API_Call1 & "print/" & DropID & API_Call2
                        End If
                        Console.WriteLine("L9 API CALL String: " & API_CALL)
                        Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                        Dim response As WebResponse = APICall.GetResponse()
                        Dim dataStream As Stream = response.GetResponseStream()
                        Dim Reader As New StreamReader(dataStream)
                        API_response = Reader.ReadToEnd()
                        If API_response.Contains("Bad") = True Then
                            StatusText = "Lane 9 incorrect Freshpak data"
                            Reader.Close()
                            response.Close()
                            Exit Do
                        End If
                        API_response = r3.Replace(API_response, "")
                        Dim tempSplit() As String = Split(API_response, ",", -1)
                        Console.WriteLine("Unsplit = " & Join(tempSplit, "|"))
                        Console.WriteLine("Length = " & tempSplit.Length)
                        Reader.Close()
                        response.Close()
                        'Concat all received data that is required for label printing and add to list
                        L9_Labels.Add(Strings.Left(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & Right(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & tempSplit(7).Substring(tempSplit(7).IndexOf(":"c) + 1) & "|" & tempSplit(5).Substring(tempSplit(5).IndexOf(":"c) + 1) & "|" & tempSplit(8).Substring(tempSplit(8).IndexOf(":"c) + 1) & "|" & tempSplit(23).Substring(tempSplit(23).IndexOf(":"c) + 1) & "|" & tempSplit(22).Substring(tempSplit(22).IndexOf(":"c) + 1) & "|" & tempSplit(9).Substring(tempSplit(9).IndexOf(":"c) + 1) & "|" & tempSplit(24).Substring(tempSplit(24).IndexOf(":"c) + 1) & "|" & tempSplit(2).Substring(tempSplit(2).IndexOf(":"c) + 1) & "|" & tempSplit(3).Substring(tempSplit(3).IndexOf(":"c) + 1) & "|" & DropID & "|" & Replace(tempSplit(20).Substring(tempSplit(20).IndexOf(":"c) + 1), "}", "") & "|" & Replace(tempSplit(47).Substring(tempSplit(47).IndexOf(":"c) + 1), "}", "") & "|" & tempSplit(46).Substring(tempSplit(46).IndexOf(":"c) + 1) & "|" & " L9" & "|" & tempSplit(My.Settings.Pallet_No_Index).Substring(tempSplit(My.Settings.Pallet_No_Index).IndexOf(":"c) + 1) & "|" & tempSplit(15).Substring(tempSplit(15).IndexOf(":"c) + 1))
                    Catch ex As Exception
                        Console.WriteLine("Lane 9 API call: " & ex.Message)
                        StatusText = "Lane 9 API call: " & ex.Message
                        Write_to_ERRORlog("L9 API call initial: " & ex.Message)
                        Exit Do
                    End Try
                Loop Until L9_Labels.Count > My.Settings.Label_QueueSize - 1 < My.Settings.Label_QueueSize + 1
                If L9_Labels.Count <> 0 Then
                    Faults(2) = False
                Else
                    StatusText = "Lane 9 API call failed"
                    Faults(2) = True
                End If
            Case 10
                DropID = L10_DropID
                Do
                    Try
                        'API Call to Freshpak for label data. Loops until queue is full.
                        If Live_Test = False Then
                            API_CALL = APITest & API_Call1 & "print/" & DropID & API_Call2
                        Else
                            API_CALL = Live_API & API_Call1 & "print/" & DropID & API_Call2
                        End If
                        Console.WriteLine("L10 API CALL String: " & API_CALL)
                        Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                        Dim response As WebResponse = APICall.GetResponse()
                        Dim dataStream As Stream = response.GetResponseStream()
                        Dim Reader As New StreamReader(dataStream)
                        API_response = Reader.ReadToEnd()
                        If API_response.Contains("Bad") = True Then
                            StatusText = "Lane 10 incorrect Freshpak data"
                            Reader.Close()
                            response.Close()
                            Exit Do
                        End If
                        API_response = r3.Replace(API_response, "")
                        Dim tempSplit() As String = Split(API_response, ",", -1)
                        Console.WriteLine("Unsplit = " & Join(tempSplit, "|"))
                        Reader.Close()
                        response.Close()
                        'Concat all received data that is required for label printing and add to list
                        L10_Labels.Add(Strings.Left(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & Right(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & tempSplit(7).Substring(tempSplit(7).IndexOf(":"c) + 1) & "|" & tempSplit(5).Substring(tempSplit(5).IndexOf(":"c) + 1) & "|" & tempSplit(8).Substring(tempSplit(8).IndexOf(":"c) + 1) & "|" & tempSplit(23).Substring(tempSplit(23).IndexOf(":"c) + 1) & "|" & tempSplit(22).Substring(tempSplit(22).IndexOf(":"c) + 1) & "|" & tempSplit(9).Substring(tempSplit(9).IndexOf(":"c) + 1) & "|" & tempSplit(24).Substring(tempSplit(24).IndexOf(":"c) + 1) & "|" & tempSplit(2).Substring(tempSplit(2).IndexOf(":"c) + 1) & "|" & tempSplit(3).Substring(tempSplit(3).IndexOf(":"c) + 1) & "|" & DropID & "|" & Replace(tempSplit(20).Substring(tempSplit(20).IndexOf(":"c) + 1), "}", "") & "|" & Replace(tempSplit(47).Substring(tempSplit(47).IndexOf(":"c) + 1), "}", "") & "|" & tempSplit(46).Substring(tempSplit(46).IndexOf(":"c) + 1) & "|" & " L10" & "|" & tempSplit(My.Settings.Pallet_No_Index).Substring(tempSplit(My.Settings.Pallet_No_Index).IndexOf(":"c) + 1) & "|" & tempSplit(15).Substring(tempSplit(15).IndexOf(":"c) + 1))
                    Catch ex As Exception
                        Console.WriteLine("Lane 10 API call: " & ex.Message)
                        StatusText = "Lane 10 API call: " & ex.Message
                        Write_to_ERRORlog("L10 API call initial: " & ex.Message)
                        Exit Do
                    End Try
                Loop Until L10_Labels.Count > My.Settings.Label_QueueSize - 1 < My.Settings.Label_QueueSize + 1
                If L10_Labels.Count <> 0 Then
                    Faults(3) = False
                Else
                    StatusText = "Lane 10 API call failed"
                    Faults(3) = True
                End If
            Case 11
                DropID = L11_DropID
                Do
                    Try
                        'API Call to Freshpak for label data. Loops until queue is full.
                        If Live_Test = False Then
                            API_CALL = APITest & API_Call1 & "print/" & DropID & API_Call2
                        Else
                            API_CALL = Live_API & API_Call1 & "print/" & DropID & API_Call2
                        End If
                        Console.WriteLine("L11 API CALL String: " & API_CALL)
                        Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                        Dim response As WebResponse = APICall.GetResponse()
                        Dim dataStream As Stream = response.GetResponseStream()
                        Dim Reader As New StreamReader(dataStream)
                        API_response = Reader.ReadToEnd()
                        If API_response.Contains("Bad") = True Then
                            StatusText = "Lane 11 incorrect Freshpak data"
                            Reader.Close()
                            response.Close()
                            Exit Do
                        End If
                        Console.WriteLine("Response = " & API_response)
                        API_response = r3.Replace(API_response, "")
                        Dim tempSplit() As String = Split(API_response, ",", -1)
                        Console.WriteLine("Unsplit = " & Join(tempSplit, "|"))
                        Console.WriteLine("Length = " & tempSplit.Length)
                        Reader.Close()
                        response.Close()
                        'Concat all received data that is required for label printing and add to list
                        L11_Labels.Add(Strings.Left(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & Right(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & tempSplit(7).Substring(tempSplit(7).IndexOf(":"c) + 1) & "|" & tempSplit(5).Substring(tempSplit(5).IndexOf(":"c) + 1) & "|" & tempSplit(8).Substring(tempSplit(8).IndexOf(":"c) + 1) & "|" & tempSplit(23).Substring(tempSplit(23).IndexOf(":"c) + 1) & "|" & tempSplit(22).Substring(tempSplit(22).IndexOf(":"c) + 1) & "|" & tempSplit(9).Substring(tempSplit(9).IndexOf(":"c) + 1) & "|" & tempSplit(24).Substring(tempSplit(24).IndexOf(":"c) + 1) & "|" & tempSplit(2).Substring(tempSplit(2).IndexOf(":"c) + 1) & "|" & tempSplit(3).Substring(tempSplit(3).IndexOf(":"c) + 1) & "|" & DropID & "|" & Replace(tempSplit(20).Substring(tempSplit(20).IndexOf(":"c) + 1), "}", "") & "|" & Replace(tempSplit(47).Substring(tempSplit(47).IndexOf(":"c) + 1), "}", "") & "|" & tempSplit(46).Substring(tempSplit(46).IndexOf(":"c) + 1) & "|" & " L11" & "|" & tempSplit(My.Settings.Pallet_No_Index).Substring(tempSplit(My.Settings.Pallet_No_Index).IndexOf(":"c) + 1) & "|" & tempSplit(15).Substring(tempSplit(15).IndexOf(":"c) + 1))
                    Catch ex As Exception
                        Console.WriteLine("Lane 11 API call: " & ex.Message)
                        StatusText = "Lane 11 API call: " & ex.Message
                        Write_to_ERRORlog("L11 API call initial: " & ex.Message)
                        Exit Do
                    End Try
                Loop Until L11_Labels.Count > My.Settings.Label_QueueSize - 1 < My.Settings.Label_QueueSize + 1
                If L11_Labels.Count <> 0 Then
                    Faults(4) = False
                Else
                    StatusText = "Lane 11 API call failed"
                    Faults(4) = True
                End If
            Case 12
                DropID = L12_DropID
                Do
                    Try
                        'API Call to Freshpak for label data. Loops until queue is full.
                        If Live_Test = False Then
                            API_CALL = APITest & API_Call1 & "print/" & DropID & API_Call2
                        Else
                            API_CALL = Live_API & API_Call1 & "print/" & DropID & API_Call2
                        End If
                        Console.WriteLine("L12 API CALL String: " & API_CALL)
                        Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                        Dim response As WebResponse = APICall.GetResponse()
                        Dim dataStream As Stream = response.GetResponseStream()
                        Dim Reader As New StreamReader(dataStream)
                        API_response = Reader.ReadToEnd()
                        If API_response.Contains("Bad") = True Then
                            StatusText = "Lane 12 incorrect Freshpak data"
                            Reader.Close()
                            response.Close()
                            Exit Do
                        End If
                        API_response = r3.Replace(API_response, "")
                        Dim tempSplit() As String = Split(API_response, ",", -1)
                        Reader.Close()
                        response.Close()
                        'Concat all received data that is required for label printing and add to list
                        L12_Labels.Add(Strings.Left(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & Right(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & tempSplit(7).Substring(tempSplit(7).IndexOf(":"c) + 1) & "|" & tempSplit(5).Substring(tempSplit(5).IndexOf(":"c) + 1) & "|" & tempSplit(8).Substring(tempSplit(8).IndexOf(":"c) + 1) & "|" & tempSplit(23).Substring(tempSplit(23).IndexOf(":"c) + 1) & "|" & tempSplit(22).Substring(tempSplit(22).IndexOf(":"c) + 1) & "|" & tempSplit(9).Substring(tempSplit(9).IndexOf(":"c) + 1) & "|" & tempSplit(24).Substring(tempSplit(24).IndexOf(":"c) + 1) & "|" & tempSplit(2).Substring(tempSplit(2).IndexOf(":"c) + 1) & "|" & tempSplit(3).Substring(tempSplit(3).IndexOf(":"c) + 1) & "|" & DropID & "|" & Replace(tempSplit(20).Substring(tempSplit(20).IndexOf(":"c) + 1), "}", "") & "|" & Replace(tempSplit(47).Substring(tempSplit(47).IndexOf(":"c) + 1), "}", "") & "|" & tempSplit(46).Substring(tempSplit(46).IndexOf(":"c) + 1) & "|" & " L12" & "|" & tempSplit(My.Settings.Pallet_No_Index).Substring(tempSplit(My.Settings.Pallet_No_Index).IndexOf(":"c) + 1) & "|" & tempSplit(15).Substring(tempSplit(15).IndexOf(":"c) + 1))
                    Catch ex As Exception
                        Console.WriteLine("Lane 12 API call: " & ex.Message)
                        StatusText = "Lane 12 API call: " & ex.Message
                        Write_to_ERRORlog("L12 API call initial: " & ex.Message)
                        Exit Do
                    End Try
                Loop Until L12_Labels.Count > My.Settings.Label_QueueSize - 1 < My.Settings.Label_QueueSize + 1
                If L12_Labels.Count <> 0 Then
                    Faults(5) = False
                Else
                    StatusText = "Lane 12 API call failed"
                    Faults(5) = True
                End If
        End Select
    End Sub
    'Split first line of data in label list into variable array for printing. Remove same from label list. Run Label building thread to create string to send to PM42
    Private Function Convert_to_send(target As Integer)
        Dim PM42 As New PM42_Print()
        Try
            If target = 7 Then
                If Variable_Array.Length > 0 Then
                    Array.Clear(Variable_Array, 0, Variable_Array.Length)
                End If
                If L7_Labels.Count <> 0 Then
                    Variable_Array = Split(L7_Labels(0), "|", -1)
                    L7_Labels.RemoveAt(0)
                    PM42.Build_Label()
                Else
                    Label_String = ""
                End If
            ElseIf target = 8 Then
                If Variable_Array.Length > 0 Then
                    Array.Clear(Variable_Array, 0, Variable_Array.Length)
                End If
                Variable_Array = Split(L8_Labels(0), "|", -1)
                L8_Labels.RemoveAt(0)
                PM42.Build_Label()
            ElseIf target = 9 Then
                If Variable_Array.Length > 0 Then
                    Array.Clear(Variable_Array, 0, Variable_Array.Length)
                End If
                If L9_Labels.Count <> 0 Then
                    Variable_Array = Split(L9_Labels(0), "|", -1)
                    L9_Labels.RemoveAt(0)
                    PM42.Build_Label()
                Else
                    Label_String = ""
                End If
            ElseIf target = 10 Then
                If Variable_Array.Length > 0 Then
                    Array.Clear(Variable_Array, 0, Variable_Array.Length)
                End If
                Variable_Array = Split(L10_Labels(0), "|", -1)
                L10_Labels.RemoveAt(0)
                PM42.Build_Label()
            ElseIf target = 11 Then
                If Variable_Array.Length > 0 Then
                    Array.Clear(Variable_Array, 0, Variable_Array.Length)
                End If
                If L11_Labels.Count <> 0 Then
                    Variable_Array = Split(L11_Labels(0), "|", -1)
                    L11_Labels.RemoveAt(0)
                    PM42.Build_Label()
                Else
                    Label_String = ""
                End If
            ElseIf target = 12 Then
                If Variable_Array.Length > 0 Then
                    Array.Clear(Variable_Array, 0, Variable_Array.Length)
                End If
                Variable_Array = Split(L12_Labels(0), "|", -1)
                L12_Labels.RemoveAt(0)
                PM42.Build_Label()
            End If
            Bytes_to_send = Encoding.UTF8.GetBytes(Label_String)
        Catch ex As Exception
            Console.WriteLine("Builder exception: " & ex.Message)
            Write_to_ERRORlog("Message builder, Lane " & target & ": " & ex.Message)
        End Try
        Return Bytes_to_send
    End Function
    'Convert received bytes from PLC/DCR to human readable text
    Private Shared Function ByteArrayToString(ByVal arrInput() As Byte) As String
        Dim i As Integer
        Dim sOutput As New StringBuilder(arrInput.Length)
        For i = 0 To arrInput.Length - 1
            sOutput.Append(arrInput(i).ToString("X2"))
        Next
        Return sOutput.ToString()
    End Function
    'Display status of connections and current count of label arrays, and current DROPIDs
    Public Sub Update_Status()
        Dim zerotime As New TimeSpan(0, 0, 0, 0)
        elapsed_time = stop_time.Subtract(start_time)
        If elapsed_time > zerotime Then
            LLT.Text = elapsed_time.TotalMilliseconds.ToString("0000.0000" & "ms")
            LLT.Visibility = 0
            LLT_Label.Visibility = 0
        Else
            LLT.Visibility = 1
            LLT_Label.Visibility = 1
        End If
        If did_busy = True Then
            Clear.IsEnabled = False
            Settings_btn.IsEnabled = False
        Else
            Clear.IsEnabled = True
            Settings_btn.IsEnabled = True
        End If
        Select Case L7prS
            Case 0
                L1pr.Text = "NOK"
            Case 1
                L1pr.Text = "OK"
        End Select
        Select Case L9prS
            Case 0
                L2pr.Text = "NOK"
            Case 1
                L2pr.Text = "OK"
        End Select
        Select Case L11prS
            Case 0
                L3pr.Text = "NOK"
            Case 1
                L3pr.Text = "OK"
        End Select
        Select Case L7plcS
            Case 0
                L1plc.Text = "IDLE"
            Case 1
                L1plc.Text = "CON"
        End Select
        Select Case L9plcS
            Case 0
                L2plc.Text = "IDLE"
            Case 1
                L2plc.Text = "CON"
        End Select
        Select Case L11plcS
            Case 0
                L3plc.Text = "IDLE"
            Case 1
                L3plc.Text = "CON"
        End Select
        Select Case L7dcrS
            Case 0
                L1DCR.Text = "NOK"
            Case 1
                L1DCR.Text = "OK"
        End Select
        Select Case L9dcrS
            Case 0
                L2DCR.Text = "NOK"
            Case 1
                L2DCR.Text = "OK"
        End Select
        Select Case L11dcrS
            Case 0
                L3DCR.Text = "NOK"
            Case 1
                L3DCR.Text = "OK"
        End Select
        L7Count.Text = L7_Labels.Count
        L8Count.Text = L8_Labels.Count
        L9Count.Text = L9_Labels.Count
        L10Count.Text = L10_Labels.Count
        L11Count.Text = L11_Labels.Count
        L12Count.Text = L12_Labels.Count
        L7DID.Text = L7_DropID
        L8DID.Text = L8_DropID
        L9DID.Text = L9_DropID
        L10DID.Text = L10_DropID
        L11DID.Text = L11_DropID
        L12DID.Text = L12_DropID
        If StatusText <> "NULL" Then
            If DISPLAY.LineCount < 100 Then
                DISPLAY.AppendText(vbCr & Date.Now & ": " & StatusText)
                DISPLAY.ScrollToEnd()
            Else
                Dim statusarray() As String
                Dim A As Integer
                Dim Z As Integer
                statusarray = Split(DISPLAY.Text, vbCr)
                Z = statusarray.Length - 1
                DISPLAY.Text = ""
                For A = 10 To Z
                    DISPLAY.AppendText(vbCr & statusarray(A))
                Next
                Array.Clear(statusarray, 0, statusarray.Length)
                DISPLAY.AppendText(vbCr & Date.Now & ": " & StatusText)
            End If
            If StatusText IsNot Nothing Then
                If StatusText.Contains("Scan") = False Then
                    Write_to_log(vbCr & Date.Now & ": " & StatusText)
                Else
                    Write_to_DCRlog(vbCr & Date.Now & ": " & StatusText)
                End If
            End If
        Else
        End If
    End Sub
    'Write events to logs.
    Public Sub Write_to_log(status As String)
        Dim WeekNo As Integer = DatePart("ww", Today, vbMonday)
        File.AppendAllText("c:\program files\Mitech\MiKiwi\Logs\Week No_" & WeekNo & ".txt", status)
    End Sub
    Public Sub Write_to_DCRlog(status As String)
        Dim WeekNo As Integer = DatePart("ww", Today, vbMonday)
        File.AppendAllText("c:\program files\Mitech\MiKiwi\Logs\Week No_" & WeekNo & " Scan Log.txt", status)
    End Sub
    Public Sub Write_to_ERRORlog(status As String)
        Dim WeekNo As Integer = DatePart("ww", Today, vbMonday)
        File.AppendAllText("c:\program files\Mitech\MiKiwi\Logs\Week No_" & WeekNo & " ErrorLog.txt", vbCr & Date.Now & ": " & status)
    End Sub
    Private Function AsciiBytesToString(ByVal bytes() As Byte) As String
        Return Encoding.ASCII.GetString(bytes)
    End Function
    'Display password box if Mitech logo is clicked.
    Private Sub Mitech_Btn_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs) Handles Mitech_Btn.MouseLeftButtonDown
        PWBox.Visibility = 0
        PWLabel.Visibility = 0
        PWBox.PasswordChar = "*"
    End Sub
    'When password box is displayed, If password is 60Cawley, open system setup, if incorrect, display contact details.
    Private Sub PWBox_KeyDown(sender As Object, e As Input.KeyEventArgs) Handles PWBox.KeyDown
        If e.Key = Key.Enter Then
            If PWBox.Password = "60Cawley" Then
                PWBox.Visibility = 1
                PWLabel.Visibility = 1
                PWBox.Password = ""
                Dim setwindow As New SettingsWindow()
                setwindow.Show()
            Else
                MsgBox("Invalid password" & vbCr & "To contact Mitech Technical support" & vbCr & "Call: 0800-648324")
                PWBox.Visibility = 1
                PWLabel.Visibility = 1
                PWBox.Password = ""
            End If
        End If
    End Sub

    Private Sub Label_Call_DoWork(sender As Object, e As DoWorkEventArgs) Handles Label_Call.DoWork
        'API CALL for labels
        Select Case Target
            Case 7
                DropID = L7_DropID
                Do
                    Try
                        'API Call to Freshpak for label data. Loops until queue is full.
                        If Live_Test = False Then
                            API_CALL = APITest & API_Call1 & "print/" & DropID & API_Call2
                        Else
                            API_CALL = Live_API & API_Call1 & "print/" & DropID & API_Call2
                        End If
                        'If Live_Test = True Then
                        Console.WriteLine("L7 API CALL String: " & API_CALL)
                            Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                            Dim response As WebResponse = APICall.GetResponse()
                            Dim dataStream As Stream = response.GetResponseStream()
                            Dim Reader As New StreamReader(dataStream)
                            API_response = Reader.ReadToEnd()
                        'Else
                        '    'API_response = File.ReadAllText("C:\Users\User\OneDrive\Customer Files\Customer Files(ASUS)\Whitehall\Logs")
                        '    Console.WriteLine("L7 API CALL String: " & API_CALL)
                        '    Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                        '    Dim response As WebResponse = APICall.GetResponse()
                        '    Dim dataStream As Stream = response.GetResponseStream()
                        '    Dim Reader As New StreamReader(dataStream)
                        '    API_response = Reader.ReadToEnd()
                        'End If
                        If API_response.Contains("Bad") = True Then
                            StatusText = "Lane 7 incorrect Freshpak data"
                            Reader.Close()
                             response.Close()
                            Exit Do
                        End If
                        API_response = r3.Replace(API_response, "")
                        Dim tempSplit() As String = Split(API_response, ",", -1)
                        Console.WriteLine("Unsplit = " & Join(tempSplit, "|"))
                        Reader.Close()
                        response.Close()
                        'Concat all received data that is required for label printing and add to list
                        L7_Labels.Add(Strings.Left(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & Right(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & tempSplit(7).Substring(tempSplit(7).IndexOf(":"c) + 1) & "|" & tempSplit(5).Substring(tempSplit(5).IndexOf(":"c) + 1) & "|" & tempSplit(8).Substring(tempSplit(8).IndexOf(":"c) + 1) & "|" & tempSplit(23).Substring(tempSplit(23).IndexOf(":"c) + 1) & "|" & tempSplit(22).Substring(tempSplit(22).IndexOf(":"c) + 1) & "|" & tempSplit(9).Substring(tempSplit(9).IndexOf(":"c) + 1) & "|" & tempSplit(24).Substring(tempSplit(24).IndexOf(":"c) + 1) & "|" & tempSplit(2).Substring(tempSplit(2).IndexOf(":"c) + 1) & "|" & tempSplit(3).Substring(tempSplit(3).IndexOf(":"c) + 1) & "|" & DropID & "|" & Replace(tempSplit(20).Substring(tempSplit(20).IndexOf(":"c) + 1), "}", "") & "|" & Replace(tempSplit(47).Substring(tempSplit(47).IndexOf(":"c) + 1), "}", "") & "|" & tempSplit(46).Substring(tempSplit(46).IndexOf(":"c) + 1) & "|" & " L7" & "|" & tempSplit(My.Settings.Pallet_No_Index).Substring(tempSplit(My.Settings.Pallet_No_Index).IndexOf(":"c) + 1) & "|" & tempSplit(15).Substring(tempSplit(15).IndexOf(":"c) + 1))
                    Catch ex As Exception
                        Console.WriteLine("Lane 7 API call: " & ex.Message)
                        StatusText = "Lane 7 API call: " & ex.Message
                        Write_to_ERRORlog("L7 API call: " & ex.Message)
                        Exit Do
                    End Try
                Loop Until L7_Labels.Count > My.Settings.Label_QueueSize - 1 < My.Settings.Label_QueueSize + 1
                If L7_Labels.Count <> 0 Then
                    Faults(0) = False
                Else
                    StatusText = "Lane 7 API call failed"
                    Faults(0) = True
                End If
            Case 8
                DropID = L8_DropID
                Do
                    Try
                        'API Call to Freshpak for label data. Loops queue is full.
                        'Change APITest to correct address when commissioning.
                        If Live_Test = False Then
                            API_CALL = APITest & API_Call1 & "print/" & DropID & API_Call2
                        Else
                            API_CALL = Live_API & API_Call1 & "print/" & DropID & API_Call2
                        End If
                        Console.WriteLine("L8 API CALL String: " & API_CALL)
                        Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                        Dim response As WebResponse = APICall.GetResponse()
                        Dim dataStream As Stream = response.GetResponseStream()
                        Dim Reader As New StreamReader(dataStream)
                        API_response = Reader.ReadToEnd()
                        If API_response.Contains("Bad") = True Then
                            StatusText = "Lane 8 incorrect Freshpak data"
                            Reader.Close()
                            response.Close()
                            Exit Do
                        End If
                        API_response = r3.Replace(API_response, "")
                        Dim tempSplit() As String = Split(API_response, ",", -1)
                        Console.WriteLine("Unsplit = " & Join(tempSplit, "|"))
                        Reader.Close()
                        response.Close()
                        'Concat all received data that is required for label printing and add to list
                        L8_Labels.Add(Strings.Left(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & Right(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & tempSplit(7).Substring(tempSplit(7).IndexOf(":"c) + 1) & "|" & tempSplit(5).Substring(tempSplit(5).IndexOf(":"c) + 1) & "|" & tempSplit(8).Substring(tempSplit(8).IndexOf(":"c) + 1) & "|" & tempSplit(23).Substring(tempSplit(23).IndexOf(":"c) + 1) & "|" & tempSplit(22).Substring(tempSplit(22).IndexOf(":"c) + 1) & "|" & tempSplit(9).Substring(tempSplit(9).IndexOf(":"c) + 1) & "|" & tempSplit(24).Substring(tempSplit(24).IndexOf(":"c) + 1) & "|" & tempSplit(2).Substring(tempSplit(2).IndexOf(":"c) + 1) & "|" & tempSplit(3).Substring(tempSplit(3).IndexOf(":"c) + 1) & "|" & DropID & "|" & Replace(tempSplit(20).Substring(tempSplit(20).IndexOf(":"c) + 1), "}", "") & "|" & Replace(tempSplit(47).Substring(tempSplit(47).IndexOf(":"c) + 1), "}", "") & "|" & tempSplit(46).Substring(tempSplit(46).IndexOf(":"c) + 1) & "|" & " L8" & "|" & tempSplit(My.Settings.Pallet_No_Index).Substring(tempSplit(My.Settings.Pallet_No_Index).IndexOf(":"c) + 1) & "|" & tempSplit(15).Substring(tempSplit(15).IndexOf(":"c) + 1))
                    Catch ex As Exception
                        Console.WriteLine("Lane 8 API call: " & ex.Message)
                        StatusText = "Lane 8 API call: " & ex.Message
                        Write_to_ERRORlog("L8 API call: " & ex.Message)
                        Exit Do
                    End Try
                Loop Until L8_Labels.Count > My.Settings.Label_QueueSize - 1 < My.Settings.Label_QueueSize + 1
                If L8_Labels.Count <> 0 Then
                    Faults(1) = False
                Else
                    StatusText = "Lane 8 API call failed"
                    Faults(1) = True
                End If
            Case 9
                DropID = L9_DropID
                Do
                    Try
                        'API Call to Freshpak for label data. Loops until queue is full.
                        If Live_Test = False Then
                            API_CALL = APITest & API_Call1 & "print/" & DropID & API_Call2
                        Else
                            API_CALL = Live_API & API_Call1 & "print/" & DropID & API_Call2
                        End If
                        Console.WriteLine("L9 API CALL String: " & API_CALL)
                        Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                        Dim response As WebResponse = APICall.GetResponse()
                        Dim dataStream As Stream = response.GetResponseStream()
                        Dim Reader As New StreamReader(dataStream)
                        API_response = Reader.ReadToEnd()
                        If API_response.Contains("Bad") = True Then
                            StatusText = "Lane 9 incorrect Freshpak data"
                            Reader.Close()
                            response.Close()
                            Exit Do
                        End If
                        API_response = r3.Replace(API_response, "")
                        Dim tempSplit() As String = Split(API_response, ",", -1)
                        Console.WriteLine("Unsplit = " & Join(tempSplit, "|"))
                        Console.WriteLine("Length = " & tempSplit.Length)
                        Reader.Close()
                        response.Close()
                        'Concat all received data that is required for label printing and add to list
                        L9_Labels.Add(Strings.Left(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & Right(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & tempSplit(7).Substring(tempSplit(7).IndexOf(":"c) + 1) & "|" & tempSplit(5).Substring(tempSplit(5).IndexOf(":"c) + 1) & "|" & tempSplit(8).Substring(tempSplit(8).IndexOf(":"c) + 1) & "|" & tempSplit(23).Substring(tempSplit(23).IndexOf(":"c) + 1) & "|" & tempSplit(22).Substring(tempSplit(22).IndexOf(":"c) + 1) & "|" & tempSplit(9).Substring(tempSplit(9).IndexOf(":"c) + 1) & "|" & tempSplit(24).Substring(tempSplit(24).IndexOf(":"c) + 1) & "|" & tempSplit(2).Substring(tempSplit(2).IndexOf(":"c) + 1) & "|" & tempSplit(3).Substring(tempSplit(3).IndexOf(":"c) + 1) & "|" & DropID & "|" & Replace(tempSplit(20).Substring(tempSplit(20).IndexOf(":"c) + 1), "}", "") & "|" & Replace(tempSplit(47).Substring(tempSplit(47).IndexOf(":"c) + 1), "}", "") & "|" & tempSplit(46).Substring(tempSplit(46).IndexOf(":"c) + 1) & "|" & " L9" & "|" & tempSplit(My.Settings.Pallet_No_Index).Substring(tempSplit(My.Settings.Pallet_No_Index).IndexOf(":"c) + 1) & "|" & tempSplit(15).Substring(tempSplit(15).IndexOf(":"c) + 1))
                    Catch ex As Exception
                        Console.WriteLine("Lane 9 API call: " & ex.Message)
                        StatusText = "Lane 9 API call: " & ex.Message
                        Write_to_ERRORlog("L9 API call: " & ex.Message)
                        Exit Do
                    End Try
                Loop Until L9_Labels.Count > My.Settings.Label_QueueSize - 1 < My.Settings.Label_QueueSize + 1
                If L9_Labels.Count <> 0 Then
                    Faults(2) = False
                Else
                    StatusText = "Lane 9 API call failed"
                    Faults(2) = True
                End If
            Case 10
                DropID = L10_DropID
                Do
                    Try
                        'API Call to Freshpak for label data. Loops until queue is full.
                        If Live_Test = False Then
                            API_CALL = APITest & API_Call1 & "print/" & DropID & API_Call2
                        Else
                            API_CALL = Live_API & API_Call1 & "print/" & DropID & API_Call2
                        End If
                        Console.WriteLine("L10 API CALL String: " & API_CALL)
                        Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                        Dim response As WebResponse = APICall.GetResponse()
                        Dim dataStream As Stream = response.GetResponseStream()
                        Dim Reader As New StreamReader(dataStream)
                        API_response = Reader.ReadToEnd()
                        If API_response.Contains("Bad") = True Then
                            StatusText = "Lane 10 incorrect Freshpak data"
                            Reader.Close()
                            response.Close()
                            Exit Do
                        End If
                        API_response = r3.Replace(API_response, "")
                        Dim tempSplit() As String = Split(API_response, ",", -1)
                        Console.WriteLine("Unsplit = " & Join(tempSplit, "|"))
                        Reader.Close()
                        response.Close()
                        'Concat all received data that is required for label printing and add to list
                        L10_Labels.Add(Strings.Left(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & Right(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & tempSplit(7).Substring(tempSplit(7).IndexOf(":"c) + 1) & "|" & tempSplit(5).Substring(tempSplit(5).IndexOf(":"c) + 1) & "|" & tempSplit(8).Substring(tempSplit(8).IndexOf(":"c) + 1) & "|" & tempSplit(23).Substring(tempSplit(23).IndexOf(":"c) + 1) & "|" & tempSplit(22).Substring(tempSplit(22).IndexOf(":"c) + 1) & "|" & tempSplit(9).Substring(tempSplit(9).IndexOf(":"c) + 1) & "|" & tempSplit(24).Substring(tempSplit(24).IndexOf(":"c) + 1) & "|" & tempSplit(2).Substring(tempSplit(2).IndexOf(":"c) + 1) & "|" & tempSplit(3).Substring(tempSplit(3).IndexOf(":"c) + 1) & "|" & DropID & "|" & Replace(tempSplit(20).Substring(tempSplit(20).IndexOf(":"c) + 1), "}", "") & "|" & Replace(tempSplit(47).Substring(tempSplit(47).IndexOf(":"c) + 1), "}", "") & "|" & tempSplit(46).Substring(tempSplit(46).IndexOf(":"c) + 1) & "|" & " L10" & "|" & tempSplit(My.Settings.Pallet_No_Index).Substring(tempSplit(My.Settings.Pallet_No_Index).IndexOf(":"c) + 1) & "|" & tempSplit(15).Substring(tempSplit(15).IndexOf(":"c) + 1))
                    Catch ex As Exception
                        Console.WriteLine("Lane 10 API call: " & ex.Message)
                        StatusText = "Lane 10 API call: " & ex.Message
                        Write_to_ERRORlog("L10 API call: " & ex.Message)
                        Exit Do
                    End Try
                Loop Until L10_Labels.Count > My.Settings.Label_QueueSize - 1 < My.Settings.Label_QueueSize + 1
                If L10_Labels.Count <> 0 Then
                    Faults(3) = False
                Else
                    StatusText = "Lane 10 API call failed"
                    Faults(3) = True
                End If
            Case 11
                DropID = L11_DropID
                Do
                    Try
                        'API Call to Freshpak for label data. Loops until queue is full.
                        If Live_Test = False Then
                            API_CALL = APITest & API_Call1 & "print/" & DropID & API_Call2
                        Else
                            API_CALL = Live_API & API_Call1 & "print/" & DropID & API_Call2
                        End If
                        Console.WriteLine("L11 API CALL String: " & API_CALL)
                        Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                        Dim response As WebResponse = APICall.GetResponse()
                        Dim dataStream As Stream = response.GetResponseStream()
                        Dim Reader As New StreamReader(dataStream)
                        API_response = Reader.ReadToEnd()
                        If API_response.Contains("Bad") = True Then
                            StatusText = "Lane 11 incorrect Freshpak data"
                            Reader.Close()
                            response.Close()
                            Exit Do
                        End If
                        Console.WriteLine("Response = " & API_response)
                        API_response = r3.Replace(API_response, "")
                        Dim tempSplit() As String = Split(API_response, ",", -1)
                        Console.WriteLine("Unsplit = " & Join(tempSplit, "|"))
                        Console.WriteLine("Length = " & tempSplit.Length)
                        Reader.Close()
                        response.Close()
                        'Concat all received data that is required for label printing and add to list
                        L11_Labels.Add(Strings.Left(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & Right(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & tempSplit(7).Substring(tempSplit(7).IndexOf(":"c) + 1) & "|" & tempSplit(5).Substring(tempSplit(5).IndexOf(":"c) + 1) & "|" & tempSplit(8).Substring(tempSplit(8).IndexOf(":"c) + 1) & "|" & tempSplit(23).Substring(tempSplit(23).IndexOf(":"c) + 1) & "|" & tempSplit(22).Substring(tempSplit(22).IndexOf(":"c) + 1) & "|" & tempSplit(9).Substring(tempSplit(9).IndexOf(":"c) + 1) & "|" & tempSplit(24).Substring(tempSplit(24).IndexOf(":"c) + 1) & "|" & tempSplit(2).Substring(tempSplit(2).IndexOf(":"c) + 1) & "|" & tempSplit(3).Substring(tempSplit(3).IndexOf(":"c) + 1) & "|" & DropID & "|" & Replace(tempSplit(20).Substring(tempSplit(20).IndexOf(":"c) + 1), "}", "") & "|" & Replace(tempSplit(47).Substring(tempSplit(47).IndexOf(":"c) + 1), "}", "") & "|" & tempSplit(46).Substring(tempSplit(46).IndexOf(":"c) + 1) & "|" & " L11" & "|" & tempSplit(My.Settings.Pallet_No_Index).Substring(tempSplit(My.Settings.Pallet_No_Index).IndexOf(":"c) + 1) & "|" & tempSplit(15).Substring(tempSplit(15).IndexOf(":"c) + 1))
                    Catch ex As Exception
                        Console.WriteLine("Lane 11 API call: " & ex.Message)
                        StatusText = "Lane 11 API call: " & ex.Message
                        Write_to_ERRORlog("L11 API call: " & ex.Message)
                        Exit Do
                    End Try
                Loop Until L11_Labels.Count > My.Settings.Label_QueueSize - 1 < My.Settings.Label_QueueSize + 1
                If L11_Labels.Count <> 0 Then
                    Faults(4) = False
                Else
                    StatusText = "Lane 11 API call failed"
                    Faults(4) = True
                End If
            Case 12
                DropID = L12_DropID
                Do
                    Try
                        'API Call to Freshpak for label data. Loops until queue is full.
                        If Live_Test = False Then
                            API_CALL = APITest & API_Call1 & "print/" & DropID & API_Call2
                        Else
                            API_CALL = Live_API & API_Call1 & "print/" & DropID & API_Call2
                        End If
                        Console.WriteLine("L12 API CALL String: " & API_CALL)
                        Dim APICall As WebRequest = WebRequest.Create(API_CALL)
                        Dim response As WebResponse = APICall.GetResponse()
                        Dim dataStream As Stream = response.GetResponseStream()
                        Dim Reader As New StreamReader(dataStream)
                        API_response = Reader.ReadToEnd()
                        If API_response.Contains("Bad") = True Then
                            StatusText = "Lane 12 incorrect Freshpak data"
                            Reader.Close()
                            response.Close()
                            Exit Do
                        End If
                        API_response = r3.Replace(API_response, "")
                        Dim tempSplit() As String = Split(API_response, ",", -1)
                        Reader.Close()
                        response.Close()
                        'Concat all received data that is required for label printing and add to list
                        L12_Labels.Add(Strings.Left(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & Right(tempSplit(21).Substring(tempSplit(21).IndexOf(":"c) + 1), 16) & "|" & tempSplit(7).Substring(tempSplit(7).IndexOf(":"c) + 1) & "|" & tempSplit(5).Substring(tempSplit(5).IndexOf(":"c) + 1) & "|" & tempSplit(8).Substring(tempSplit(8).IndexOf(":"c) + 1) & "|" & tempSplit(23).Substring(tempSplit(23).IndexOf(":"c) + 1) & "|" & tempSplit(22).Substring(tempSplit(22).IndexOf(":"c) + 1) & "|" & tempSplit(9).Substring(tempSplit(9).IndexOf(":"c) + 1) & "|" & tempSplit(24).Substring(tempSplit(24).IndexOf(":"c) + 1) & "|" & tempSplit(2).Substring(tempSplit(2).IndexOf(":"c) + 1) & "|" & tempSplit(3).Substring(tempSplit(3).IndexOf(":"c) + 1) & "|" & DropID & "|" & Replace(tempSplit(20).Substring(tempSplit(20).IndexOf(":"c) + 1), "}", "") & "|" & Replace(tempSplit(47).Substring(tempSplit(47).IndexOf(":"c) + 1), "}", "") & "|" & tempSplit(46).Substring(tempSplit(46).IndexOf(":"c) + 1) & "|" & " L12" & "|" & tempSplit(My.Settings.Pallet_No_Index).Substring(tempSplit(My.Settings.Pallet_No_Index).IndexOf(":"c) + 1) & "|" & tempSplit(15).Substring(tempSplit(15).IndexOf(":"c) + 1))
                    Catch ex As Exception
                        Console.WriteLine("Lane 12 API call: " & ex.Message)
                        StatusText = "Lane 12 API call: " & ex.Message
                        Write_to_ERRORlog("L12 API call: " & ex.Message)
                        Exit Do
                    End Try
                Loop Until L12_Labels.Count > My.Settings.Label_QueueSize - 1 < My.Settings.Label_QueueSize + 1
                If L12_Labels.Count <> 0 Then
                    Faults(5) = False
                Else
                    StatusText = "Lane 12 API call failed"
                    Faults(5) = True
                End If
        End Select
    End Sub

    Private Sub Label_Call_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles Label_Call.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            'if BackgroundWorker terminated due to error
            Label_Call.Dispose()
        ElseIf e.Cancelled Then
            Label_Call.Dispose()
        Else
            Label_Call.Dispose()
            Console.WriteLine("Label call done")
        End If
    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        Tick.Stop()
        CommsTimer.Stop()
        L7_Labels.Clear()
        L8_Labels.Clear()
        L9_Labels.Clear()
        L10_Labels.Clear()
        L11_Labels.Clear()
        L12_Labels.Clear()
        FirstSTD_label = {True, True, True, True, True, True}
        FirstSTDPAL_label = {True, True, True, True, True, True}
        FirstREWE_label = {True, True, True, True, True, True}
        FirstREWEPAL_label = {True, True, True, True, True, True}
        Faults = {False, False, False, False, False, False}
        First_cycle = True
        If DropID_Call.IsBusy = False Then
            DropID_Call.RunWorkerAsync()
        End If
    End Sub
    Private Sub CommsTimer_Tick(sender As Object, e As EventArgs) Handles CommsTimer.Tick
        'Check if Lane 7 has system allocated, and start Servers and Clients if not already busy.
        Try
            If L7sys <> 0 And Set_open = False Then
                If Lane7_PLC_Server.IsBusy = False Then
                    Lane7_PLC_Server.RunWorkerAsync()
                End If
                If Lane7_DCR_Server.IsBusy = False Then
                    Lane7_DCR_Server.RunWorkerAsync()
                End If
            ElseIf L7sys = 0 And Set_open = False Then
                'If no system allocated, set all status bits off
                L7plcS = 0
                L7prS = 0
                L7dcrS = 0
            End If
            'Check if Lane 9 has system allocated, and start Servers and Clients if not already busy.
            If L9sys <> 0 And Set_open = False Then
                If Lane9_PLC_Server.IsBusy = False Then
                    Lane9_PLC_Server.RunWorkerAsync()
                End If
                If Lane9_DCR_Server.IsBusy = False Then
                    Lane9_DCR_Server.RunWorkerAsync()
                End If
            ElseIf L9sys = 0 And Set_open = False Then
                'If no system allocated, set all status bits off
                L9plcS = 0
                L9prS = 0
                L9dcrS = 0
            End If
            'Check if Lane 11 has system allocated, and start Servers and Clients if not already busy.
            If L11sys <> 0 And Set_open = False Then
                If Lane11_PLC_Server.IsBusy = False Then
                    Lane11_PLC_Server.RunWorkerAsync()
                End If
                If Lane11_DCR_Server.IsBusy = False Then
                    Lane11_DCR_Server.RunWorkerAsync()
                End If
            ElseIf L11sys = 0 And Set_open = False Then
                'If no system allocated, set all status bits off
                L11plcS = 0
                L11prS = 0
                L11dcrS = 0
            End If
        Catch ex As Exception
            Console.WriteLine("Comms exception: " & ex.Message)
            Write_to_ERRORlog("Comms error: " & ex.Message)
        End Try
    End Sub

End Class
