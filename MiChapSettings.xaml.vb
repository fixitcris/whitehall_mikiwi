﻿Imports System.Linq.Expressions
Imports System.Net
Imports System.Security.Permissions
Imports System.Threading
Imports System.Windows.Forms
Imports System.Windows.Threading

Public Class SettingsWindow
    Private Sub SettingsWindow_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        L1_Printer_IP_TB.Text = My.Settings.MC1_Printer_IP
        L1_PLC_PORT_TB.Text = My.Settings.MC1_PLC_PORT
        L1_DCR_PORT_TB.Text = My.Settings.MC1_DCR_PORT
        L2_Printer_IP_TB.Text = My.Settings.MC2_Printer_IP
        L2_PLC_PORT_TB.Text = My.Settings.MC2_PLC_PORT
        L2_DCR_PORT_TB.Text = My.Settings.MC2_DCR_PORT
        L3_Printer_IP_TB.Text = My.Settings.MC3_Printer_IP
        L3_PLC_PORT_TB.Text = My.Settings.MC3_PLC_PORT
        L3_DCR_PORT_TB.Text = My.Settings.MC3_DCR_PORT
        If Live_Test = False Then
            Test_mode.IsChecked = True
            Live_mode.IsChecked = False
        Else
            Live_mode.IsChecked = True
            Test_mode.IsChecked = False
        End If
    End Sub
    Private Sub Home_BTN_Click(sender As Object, e As RoutedEventArgs) Handles Home_BTN.Click
        My.Settings.Live_mode = Live_Test
        Close()
    End Sub
    Public Function IsAddressValid(addrString As String) As Boolean
        Dim address As IPAddress = Nothing
        Return IPAddress.TryParse(addrString, address)
    End Function
    Private Sub L1_Printer_IP_TB_LostKeyboardFocus(sender As Object, e As KeyboardFocusChangedEventArgs) Handles L1_Printer_IP_TB.LostKeyboardFocus
        If IsAddressValid(L1_Printer_IP_TB.Text) = False Then
            MsgBox("Invalid Ip address")
            L1_Printer_IP_TB.Text = My.Settings.MC1_Printer_IP
        Else
            My.Settings.MC1_Printer_IP = L1_Printer_IP_TB.Text
        End If
    End Sub
    Private Sub L2_Printer_IP_TB_LostKeyboardFocus(sender As Object, e As KeyboardFocusChangedEventArgs) Handles L2_Printer_IP_TB.LostKeyboardFocus
        If IsAddressValid(L2_Printer_IP_TB.Text) = False Then
            MsgBox("Invalid Ip address")
            L2_Printer_IP_TB.Text = My.Settings.MC2_Printer_IP
        Else
            My.Settings.MC2_Printer_IP = L2_Printer_IP_TB.Text
        End If
    End Sub
    Private Sub L3_Printer_IP_TB_LostKeyboardFocus(sender As Object, e As KeyboardFocusChangedEventArgs) Handles L3_Printer_IP_TB.LostKeyboardFocus
        If IsAddressValid(L3_Printer_IP_TB.Text) = False Then
            MsgBox("Invalid Ip address")
            L3_Printer_IP_TB.Text = My.Settings.MC3_Printer_IP
        Else
            My.Settings.MC3_Printer_IP = L3_Printer_IP_TB.Text
        End If
    End Sub
    Private Sub L1_PLC_PORT_TB_TextChanged(sender As Object, e As TextChangedEventArgs) Handles L1_PLC_PORT_TB.TextChanged
        My.Settings.MC1_PLC_PORT = L1_PLC_PORT_TB.Text
    End Sub
    Private Sub L2_PLC_PORT_TB_TextChanged(sender As Object, e As TextChangedEventArgs) Handles L2_PLC_PORT_TB.TextChanged
        My.Settings.MC2_PLC_PORT = L2_PLC_PORT_TB.Text
    End Sub
    Private Sub L3_PLC_PORT_TB_TextChanged(sender As Object, e As TextChangedEventArgs) Handles L3_PLC_PORT_TB.TextChanged
        My.Settings.MC3_PLC_PORT = L3_PLC_PORT_TB.Text
    End Sub
    Private Sub L1_DCR_PORT_TB_TextChanged(sender As Object, e As TextChangedEventArgs) Handles L1_DCR_PORT_TB.TextChanged
        My.Settings.MC1_DCR_PORT = L1_DCR_PORT_TB.Text
    End Sub
    Private Sub L2_DCR_PORT_TB_TextChanged(sender As Object, e As TextChangedEventArgs) Handles L2_DCR_PORT_TB.TextChanged
        My.Settings.MC2_DCR_PORT = L2_DCR_PORT_TB.Text
    End Sub
    Private Sub L3_DCR_PORT_TB_TextChanged(sender As Object, e As TextChangedEventArgs) Handles L3_DCR_PORT_TB.TextChanged
        My.Settings.MC3_DCR_PORT = L3_DCR_PORT_TB.Text
    End Sub

    Private Sub Test_mode_Checked(sender As Object, e As RoutedEventArgs) Handles Test_mode.Checked
        Live_Test = False
        Live_mode.IsChecked = False
    End Sub

    Private Sub Live_mode_Checked(sender As Object, e As RoutedEventArgs) Handles Live_mode.Checked
        Live_Test = True
        Test_mode.IsChecked = False
    End Sub

    Private Sub Live_mode_Click(sender As Object, e As RoutedEventArgs) Handles Live_mode.Click
        Dim answer As Integer
        answer = MsgBox("Switch to Go Live, are you sure?", vbQuestion + vbYesNo + vbDefaultButton1, "Go Live")
        If answer = vbYes Then
            Live_mode.IsChecked = True
            Test_mode.IsChecked = False
            Dim main As New MainWindow
            main.Tick.Stop()
            First_cycle = True
            Thread.Sleep(2000)
            If main.DropID_Call.IsBusy = False Then
                main.DropID_Call.RunWorkerAsync()
            End If
            main.Write_to_log(vbCr & Date.Now & ": Switched to Live mode")
        ElseIf answer = vbNo Then
            Live_mode.IsChecked = False
            Test_mode.IsChecked = True
        End If
    End Sub

    Private Sub Test_mode_Click(sender As Object, e As RoutedEventArgs) Handles Test_mode.Click
        Dim answer As Integer
        answer = MsgBox("Switch to Test Environment, are you sure?", vbQuestion + vbYesNo + vbDefaultButton1, "Test environment")
        If answer = vbYes Then
            Live_mode.IsChecked = False
            Test_mode.IsChecked = True
            Dim main As New MainWindow
            main.Tick.Stop()
            First_cycle = True
            Thread.Sleep(2000)
            If main.DropID_Call.IsBusy = False Then
                main.DropID_Call.RunWorkerAsync()
            End If
            main.Write_to_log(vbCr & Date.Now & ": Switched to Test mode")
        ElseIf answer = vbNo Then
            Live_mode.IsChecked = True
            Test_mode.IsChecked = False
        End If
    End Sub
End Class
