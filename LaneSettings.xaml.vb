﻿Public Class LaneSettings

    Private Sub LaneSettings_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Set_open = True
        L7sys = My.Settings.L1system
        L9sys = My.Settings.L2system
        L11sys = My.Settings.L3system
        L1_Sys_Box.Items.Clear()
        L1_Sys_Box.Items.Add("Not assigned")
        L1_Sys_Box.Items.Add("MC 1")
        L1_Sys_Box.Items.Add("MC 2")
        L1_Sys_Box.Items.Add("MC 3")
        L1_Sys_Box.SelectedIndex = L7sys
        L2_Sys_Box.Items.Clear()
        L2_Sys_Box.Items.Add("Not assigned")
        L2_Sys_Box.Items.Add("MC 1")
        L2_Sys_Box.Items.Add("MC 2")
        L2_Sys_Box.Items.Add("MC 3")
        L2_Sys_Box.SelectedIndex = L9sys
        L3_Sys_Box.Items.Clear()
        L3_Sys_Box.Items.Add("Not assigned")
        L3_Sys_Box.Items.Add("MC 1")
        L3_Sys_Box.Items.Add("MC 2")
        L3_Sys_Box.Items.Add("MC 3")
        L3_Sys_Box.SelectedIndex = L11sys
        If My.Settings.Split_Lanes = True Then
            MANUAL_BTN.IsChecked = True
            ROBOT_BTN.IsChecked = False
        Else
            MANUAL_BTN.IsChecked = False
            ROBOT_BTN.IsChecked = True
        End If
    End Sub
    Private Sub L1_Sys_Box_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles L1_Sys_Box.SelectionChanged
        L7sys = L1_Sys_Box.SelectedIndex
        If L7sys <> 0 Then
            If L9sys = L7sys Then
                Dim answer As Integer
                answer = MsgBox("System " & L7sys & " already assigned to Lanes 8 & 9," & vbCr & "Reassign to lanes 7 & 8?", vbQuestion + vbYesNo + vbDefaultButton2, "Assign system")
                If answer = vbYes Then
                    L7sys = L1_Sys_Box.SelectedIndex
                    L9sys = 0
                    L2_Sys_Box.SelectedIndex = 0
                Else
                    L7sys = 0
                    L1_Sys_Box.SelectedIndex = 0
                End If
            End If
            If L11sys = L7sys Then
                Dim answer2 As Integer
                answer2 = MsgBox("System " & L7sys & " already assigned to Lanes 11 & 12," & vbCr & "Reassign to lanes 7 & 8?", vbQuestion + vbYesNo + vbDefaultButton2, "Assign system")
                If answer2 = vbYes Then
                    L7sys = L1_Sys_Box.SelectedIndex
                    L11sys = 0
                    L3_Sys_Box.SelectedIndex = 0
                Else
                    L7sys = 0
                    L1_Sys_Box.SelectedIndex = 0
                End If
            End If
        End If
        My.Settings.L1system = L7sys
    End Sub

    Private Sub L2_Sys_Box_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles L2_Sys_Box.SelectionChanged
        L9sys = L2_Sys_Box.SelectedIndex
        If L9sys <> 0 Then
            If L7sys = L9sys Then
                Dim answer As Integer
                answer = MsgBox("System " & L9sys & " already assigned to Lanes 7 & 8," & vbCr & "Reassign to lanes 9 & 10?", vbQuestion + vbYesNo + vbDefaultButton2, "Assign system")
                If answer = vbYes Then
                    L9sys = L2_Sys_Box.SelectedIndex
                    L7sys = 0
                    L1_Sys_Box.SelectedIndex = 0
                Else
                    L9sys = 0
                    L2_Sys_Box.SelectedIndex = 0
                End If
            End If
            If L11sys = L9sys Then
                Dim answer2 As Integer
                answer2 = MsgBox("System " & L9sys & " already assigned to Lanes 11 & 12," & vbCr & "Reassign to lanes 9 & 10?", vbQuestion + vbYesNo + vbDefaultButton2, "Assign system")
                If answer2 = vbYes Then
                    L9sys = L2_Sys_Box.SelectedIndex
                    L11sys = 0
                    L3_Sys_Box.SelectedIndex = 0
                Else
                    L9sys = 0
                    L2_Sys_Box.SelectedIndex = 0
                End If
            End If
        End If
        My.Settings.L2system = L9sys
    End Sub

    Private Sub L3_Sys_Box_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles L3_Sys_Box.SelectionChanged
        L11sys = L3_Sys_Box.SelectedIndex
        If L11sys <> 0 Then
            If L9sys = L11sys Then
                Dim answer As Integer
                answer = MsgBox("System " & L11sys & " already assigned to Lanes 9 & 10," & vbCr & "Reassign to lanes 11 & 12?", vbQuestion + vbYesNo + vbDefaultButton2, "Assign system")
                If answer = vbYes Then
                    L11sys = L3_Sys_Box.SelectedIndex
                    L9sys = 0
                    L2_Sys_Box.SelectedIndex = 0
                Else
                    L11sys = 0
                    L3_Sys_Box.SelectedIndex = 0
                End If
            End If
            If L7sys = L11sys Then
                Dim answer2 As Integer
                answer2 = MsgBox("System " & L11sys & " already assigned to Lanes 7 & 8," & vbCr & "Reassign to lanes 11 & 12?", vbQuestion + vbYesNo + vbDefaultButton2, "Assign system")
                If answer2 = vbYes Then
                    L11sys = L3_Sys_Box.SelectedIndex
                    L7sys = 0
                    L1_Sys_Box.SelectedIndex = 0
                Else
                    L11sys = 0
                    L3_Sys_Box.SelectedIndex = 0
                End If
            End If
        End If
        My.Settings.L3system = L11sys
    End Sub

    Private Sub Home_BTN_Click(sender As Object, e As RoutedEventArgs) Handles Home_BTN.Click
        My.Settings.L1system = L7sys
        My.Settings.L2system = L9sys
        My.Settings.L3system = L11sys
        Set_open = False
        My.Settings.Save()
        Close()
    End Sub

    Private Sub MANUAL_BTN_Checked(sender As Object, e As RoutedEventArgs) Handles MANUAL_BTN.Checked
    End Sub

    Private Sub ROBOT_BTN_Checked(sender As Object, e As RoutedEventArgs) Handles ROBOT_BTN.Checked
    End Sub
End Class
