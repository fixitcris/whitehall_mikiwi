﻿Imports System.Net
Imports System.Net.Sockets
Public Module Globals
    'Packhouse code
    Public PHCode As String = """4QPX""" & vbCr
    '-----------------------------------------------
    'API URLS
    Public API_Call1 As String = "api/printserver/"
    Public API_Call2 As String = "/EAN/1"
    Public APITest As String = "http://rslzgstst.westeurope.cloudapp.azure.com:9090/"
    Public API_response As String
    Public Live_API As String = "http://192.168.31.90:9090/"
    '-----------------------------------------------
    'Label data arrays
    Public L7_Labels As New List(Of String)
    Public L8_Labels As New List(Of String)
    Public L9_Labels As New List(Of String)
    Public L10_Labels As New List(Of String)
    Public L11_Labels As New List(Of String)
    Public L12_Labels As New List(Of String)
    '-----------------------------------------------
    'Request flags
    Public L7Request As Boolean = False
    Public L9Request As Boolean = False
    Public L11Request As Boolean = False
    Public L8Request As Boolean = False
    Public L10Request As Boolean = False
    Public L12Request As Boolean = False
    '-----------------------------------------------
    Public L7Scanned, L9Scanned, L11Scanned As String
    Public Bytes_to_send() As Byte
    'ACKs to PLC 0600 for normal labelling, 0601 for first label in cycle(takes longer for printer to process)
    Public ACK0() As Byte = {&H6, &H0}
    Public ACK1() As Byte = {&H6, &H1}
    '-----------------------------------------------
    Public First_cycle As Boolean = False
    Public FirstSTD_label(3) As Boolean
    Public FirstSTDPAL_label(3) As Boolean
    Public FirstREWE_label(3) As Boolean
    Public FirstREWEPAL_label(3) As Boolean
    Public L7prS, L7plcS, L9prS, L9plcS, L11prS, L11plcS, L7dcrS, L9dcrS, L11dcrS, Mitech, Set_open As Boolean
    Public L7sys, L9sys, L11sys, Target, Printersys As Integer
    'Drop IDs - fixed for Whitehall
    '-----------------------------------------------
    Public L7_DropID As String = "601"
    Public L8_DropID As String = "602"
    Public L9_DropID As String = "603"
    Public L10_DropID As String = "604"
    Public L11_DropID As String = "605"
    Public L12_DropID As String = "606"
    '-----------------------------------------------
    Public L7ipstr, L9ipstr, L11ipstr As String
    Public API_CALL, DropID, Label_String As String
    Public Grader As String
    Public StatusText As String
    Public Variable_Array(16) As String
    Public L7ip, L9ip, L11ip As IPAddress
    Public Server1, Server2, Server3, Server4, Server5, Server6 As TcpListener
    Public Port1, Port2, Port3, Port4, Port5, Port6 As Integer
    Public localAddr As IPAddress = IPAddress.Any
    Public client1, client2, client3, client4, client5, client6 As TcpClient
    Public Live_Test, did_busy As Boolean
    Public Faults() As Boolean = {False, False, False, False, False, False}
    Public Manual() As Boolean = {False, False, False}
    Public Lay() As Integer = {False, False, False}

    'TIMERS FOR TESTING SPEED
    Public start_time As DateTime
    Public stop_time As DateTime
    Public elapsed_time As TimeSpan
End Module

