﻿Imports System.IO
Imports System.Threading

Public Class PM42_Print
    Public Function Build_Label()
        Try
            Dim LANE_requesting As String = Variable_Array(15)
            If LANE_requesting = "L7" Or LANE_requesting = "L8" Then
                Printersys = 0
            ElseIf LANE_requesting = "L9" Or LANE_requesting = "L10" Then
                Printersys = 1
            ElseIf LANE_requesting = "L11" Or LANE_requesting = "L12" Then
                Printersys = 2
            End If
            If Variable_Array(16).Contains("ull") = True Then
                Manual(Printersys) = False
            Else
                Manual(Printersys) = True
            End If
            If Manual(Printersys) = True Then
                If Variable_Array(14) = "" Then
                    Lay(Printersys) = 1
                Else
                    Lay(Printersys) = 3
                End If
            Else
                If Variable_Array(14) = "" Then
                    Lay(Printersys) = 2
                Else
                    Lay(Printersys) = 4
                End If
            End If
            Label_String = ""
            Console.WriteLine("Requesting Lane: " & Variable_Array(15) & " Layout: " & Lay(Printersys))
            If Lay(Printersys) = 1 Then
                Label_String = "Input OFF" & vbCr & "Format Input ""!"", ""@"", ""&""" & vbCr & "INPUT ON" & vbCr & "LAYOUT RUN ""STDPAL.LAY""" & vbCr & "!" & Variable_Array(0) & Variable_Array(1) & "&" & "(" & Left(Variable_Array(0), 2) & ")" & Right(Variable_Array(0), 14) & "&" & "(" & Left(Variable_Array(1), 2) & ")" & Right(Variable_Array(1), 14) & "&" & Variable_Array(2) & "&" & Variable_Array(3) & "&" & Variable_Array(5) & "&" & Variable_Array(7) & "&" & Variable_Array(6) & "&" & Variable_Array(4) & "&" & Variable_Array(9) & "&" & Variable_Array(10) & "&" & Left(Variable_Array(8), 4) & "&" & Variable_Array(13) & "&" & " " & " MiChap v1.0" & "&" & "DID " & Variable_Array(11) & "&" & Variable_Array(12) & "&" & Variable_Array(16) & "&" & Variable_Array(17) & "&" & "@" & vbCr & "PF" & vbCr
                FirstSTDPAL_label(Printersys) = False
                FirstSTD_label(Printersys) = True
                FirstREWEPAL_label(Printersys) = True
                FirstREWE_label(Printersys) = True
            ElseIf Lay(Printersys) = 2 Then
                Label_String = "Input OFF" & vbCr & "Format Input ""!"", ""@"", ""&""" & vbCr & "INPUT ON" & vbCr & "LAYOUT RUN ""STD.LAY""" & vbCr & "!" & Variable_Array(0) & Variable_Array(1) & "&" & "(" & Left(Variable_Array(0), 2) & ")" & Right(Variable_Array(0), 14) & "&" & "(" & Left(Variable_Array(1), 2) & ")" & Right(Variable_Array(1), 14) & "&" & Variable_Array(2) & "&" & Variable_Array(3) & "&" & Variable_Array(5) & "&" & Variable_Array(7) & "&" & Variable_Array(6) & "&" & Variable_Array(4) & "&" & Variable_Array(9) & "&" & Variable_Array(10) & "&" & Left(Variable_Array(8), 4) & "&" & Variable_Array(13) & "&" & Variable_Array(15) & " MiChap v1.0" & "&" & "DID " & Variable_Array(11) & "&" & Variable_Array(12) & "&" & "@" & vbCr & "PF" & vbCr
                FirstSTDPAL_label(Printersys) = True
                FirstSTD_label(Printersys) = False
                FirstREWEPAL_label(Printersys) = True
                FirstREWE_label(Printersys) = True
            ElseIf Lay(Printersys) = 3 Then
                Label_String = "Input OFF" & vbCr & "Format Input ""!"", ""@"", ""&""" & vbCr & "INPUT ON" & vbCr & "LAYOUT RUN ""REWEPAL.LAY""" & vbCr & "!" & Variable_Array(0) & Variable_Array(1) & "&" & "(" & Left(Variable_Array(0), 2) & ")" & Right(Variable_Array(0), 14) & "&" & "(" & Left(Variable_Array(1), 2) & ")" & Right(Variable_Array(1), 14) & "&" & Variable_Array(2) & "&" & Variable_Array(3) & "&" & Variable_Array(5) & "&" & Variable_Array(7) & "&" & Variable_Array(6) & "&" & Variable_Array(4) & "&" & Variable_Array(9) & "&" & Variable_Array(10) & "&" & Left(Variable_Array(8), 4) & "&" & Variable_Array(13) & "&" & Variable_Array(15) & " MiChap v1.0" & "&" & "DID " & Variable_Array(11) & "&" & Left(Variable_Array(14), 12) & "&" & Left(Variable_Array(14), 1) & "&" & Right(Left(Variable_Array(14), 7), 6) & "&" & Right(Left(Variable_Array(14), 13), 6) & "&" & Right(Variable_Array(14), 1) & "&" & Variable_Array(12) & "&" & Variable_Array(16) & "&" & Variable_Array(17) & "&" & "@" & vbCr & "PF" & vbCr
                FirstSTDPAL_label(Printersys) = True
                FirstSTD_label(Printersys) = True
                FirstREWEPAL_label(Printersys) = False
                FirstREWE_label(Printersys) = True
            Else
                Label_String = "Input OFF" & vbCr & "Format Input ""!"", ""@"", ""&""" & vbCr & "INPUT ON" & vbCr & "LAYOUT RUN ""REWE.LAY""" & vbCr & "!" & Variable_Array(0) & Variable_Array(1) & "&" & "(" & Left(Variable_Array(0), 2) & ")" & Right(Variable_Array(0), 14) & "&" & "(" & Left(Variable_Array(1), 2) & ")" & Right(Variable_Array(1), 14) & "&" & Variable_Array(2) & "&" & Variable_Array(3) & "&" & Variable_Array(5) & "&" & Variable_Array(7) & "&" & Variable_Array(6) & "&" & Variable_Array(4) & "&" & Variable_Array(9) & "&" & Variable_Array(10) & "&" & Left(Variable_Array(8), 4) & "&" & Variable_Array(13) & "&" & Variable_Array(15) & " MiChap v1.0" & "&" & "DID " & Variable_Array(11) & "&" & Left(Variable_Array(14), 12) & "&" & Left(Variable_Array(14), 1) & "&" & Right(Left(Variable_Array(14), 7), 6) & "&" & Right(Left(Variable_Array(14), 13), 6) & "&" & Right(Variable_Array(14), 1) & "&" & Variable_Array(12) & "&" & "@" & vbCr & "PF" & vbCr
                FirstSTDPAL_label(Printersys) = True
                FirstSTD_label(Printersys) = True
                FirstREWEPAL_label(Printersys) = True
                FirstREWE_label(Printersys) = False
            End If
        Catch ex As Exception
            'Write_to_ERRORlog("PM42 error: " & ex.Message)
        End Try
        Return Label_String
    End Function
End Class
